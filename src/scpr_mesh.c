/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "scpr.h"
#include "scpr_c.h"

#include <rsys/mem_allocator.h>
#include <rsys/logger.h>

#include <polygon.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE Clipper2Lib::ClipType
scpr_operation_to_clip_type(const enum scpr_operation op)
{
  Clipper2Lib::ClipType ctype;
  switch(op) {
    case SCPR_AND: ctype = Clipper2Lib::ClipType::Intersection; break;
    case SCPR_SUB: ctype = Clipper2Lib::ClipType::Difference; break;
    default: FATAL("Unreachable code\n"); break;
  }
  return ctype;
}

static INLINE int
aabb_intersect
  (const int64_t lower0[2],
   const int64_t upper0[2],
   const int64_t lower1[2],
   const int64_t upper1[2])
{
  ASSERT(lower0 && upper0 && lower1 && upper1);
  return lower0[0] < upper1[0]
      && lower0[1] < upper1[1]
      && lower1[0] < upper0[0]
      && lower1[1] < upper0[1];
}

static INLINE int
aabb_is_degenerated(const int64_t lower[2], const int64_t upper[2])
{
  ASSERT(lower && upper);
  return lower[0] >= upper[0] || lower[1] >= upper[1];
}

static void
triangle_compute_aabb
  (int64_t tri[3][2],
   int64_t lower[2],
   int64_t upper[2])
{
  size_t ivert;
  ASSERT(tri && lower && upper);

  lower[0] = lower[1] = INT64_MAX;
  upper[0] = upper[1] = INT64_MIN;
  FOR_EACH(ivert, 0, 3) {
    int i;
    for(i = 0; i < 2; i++) {
      if(lower[i] > tri[ivert][i]) lower[i] = tri[ivert][i];
      if(upper[i] < tri[ivert][i]) upper[i] = tri[ivert][i];
    }
  }
}

static res_T
register_vertex
  (const int64_t pos[2],
   struct darray_int64* coords,
   struct darray_uint32* indices,
   struct htable_vertex* vertices)
{
  struct vertex v;
  uint32_t* pid, id;
  unsigned i;
  res_T res = RES_OK;
  ASSERT(pos && coords && indices && vertices);

  for(i = 0; i < 2; i++) v.pos[i] = pos[i];

  v.pos[0] = v.pos[0];
  v.pos[1] = v.pos[1];

  pid = htable_vertex_find(vertices, &v);

  if(pid) {
    id = *pid;
  } else {
    const size_t count = darray_int64_size_get(coords);

    ASSERT(count <= UINT32_MAX);
    ERR(darray_int64_resize(coords, count+2/*#coords*/));
    for(i = 0; i < 2; i++) darray_int64_data_get(coords)[count+i] = pos[i];

    id = (uint32_t)count / 2;
    ERR(htable_vertex_set(vertices, &v, &id));
  }

  ERR(darray_uint32_push_back(indices, &id));

exit:
  return res;
error:
  goto exit;
}

static FINLINE res_T
register_triangle
  (int64_t tri[3][2],
   struct darray_int64* coords, /* Vertex buffer */
   struct darray_uint32* indices, /* Index buffer */
   struct htable_vertex* vertices) /* Map a vertex to its index */
{
  size_t ivert;
  res_T res = RES_OK;
  ASSERT(tri && coords && indices && vertices);
  FOR_EACH(ivert, 0, 3) {
    ERR(register_vertex(tri[ivert], coords, indices, vertices));
  }
exit:
  return RES_OK;
error:
  goto exit;
}


static res_T
register_paths
  (struct scpr_device* dev,
   const Clipper2Lib::Paths64& paths,
   struct darray_int64* coords, /* Vertex buffer */
   struct darray_uint32* indices, /* Index buffer */
   struct htable_vertex* vertices, /* Map a vertex to its index */
   struct polygon* polygon) /* Used to triangulate the clipped polygons */
{
  size_t ivert;
  size_t ipath;
  res_T res = RES_OK;
  ASSERT(coords && indices && vertices);

  FOR_EACH(ipath, 0, paths.size()) {
    if(paths[ipath].size() == 3) {
      FOR_EACH(ivert, 0, 3) {
        int64_t pos[2];
        pos[0] = paths[ipath][ivert].x;
        pos[1] = paths[ipath][ivert].y;
        ERR(register_vertex(pos, coords, indices, vertices));
      }
    } else {
      /* Triangulate the clipped primitive */
      const uint32_t* ids;
      uint32_t nids;

      /* Define the contour of the polygon to triangulate */
      POLYGON(clear(polygon));
      FOR_EACH(ivert, 0, paths[ipath].size()) {
        float fpos[3] = {0.f, 0.f, 0.f};
        double posd[2];
        int64_t pos64[2];

        pos64[0] = paths[ipath][ivert].x;
        pos64[1] = paths[ipath][ivert].y;
        SCPR(device_unscale(dev, pos64, 2, posd));

        fpos[0] = (float)posd[0], fpos[1] = (float)posd[1];
        ERR(polygon_vertex_add(polygon, fpos));
      }
      ERR(polygon_triangulate(polygon, &ids, &nids));

      FOR_EACH(ivert, 0, nids) {
        float fpos[3];
        int64_t pos64[2];
        double pos[2];
        POLYGON(vertex_get(polygon, ids[ivert], fpos));
        pos[0] = (double)fpos[0];
        pos[1] = (double)fpos[1];
        SCPR(device_scale(dev, pos, 2, pos64));
        ERR(register_vertex(pos64, coords, indices, vertices));
      }
    }
  }
exit:
  return res;
error:
  goto exit;
}

static void
mesh_compute_aabb
  (const struct scpr_mesh* mesh,
   int64_t lower[2],
   int64_t upper[2])
{
  size_t itri, ntris, i;

  SCPR(mesh_get_triangles_count(mesh, &ntris));
  lower[0] = lower[1] = INT64_MAX;
  upper[0] = upper[1] = INT64_MIN;

  FOR_EACH(itri, 0, ntris) {
    size_t ids[3], ivert;
    SCPR(mesh_get_indices(mesh, itri, ids));
    FOR_EACH(ivert, 0, 3) {
      double pos[2];
      int64_t pos64[2];
      SCPR(mesh_get_position(mesh, ids[ivert], pos));
      SCPR(device_scale(mesh->device, pos, 2, pos64));
      for(i = 0; i < 2; i++) {
        if(lower[i] > pos64[i]) lower[i] = pos64[i];
        if(upper[i] < pos64[i]) upper[i] = pos64[i];
      }
    }
  }
}

static void
mesh_release(ref_T* ref)
{
  struct scpr_mesh* mesh;
  struct mem_allocator* allocator;
  ASSERT(ref);
  mesh = CONTAINER_OF(ref, struct scpr_mesh, ref);
  allocator = mesh->device->allocator;
  SCPR(device_ref_put(mesh->device));
  darray_int64_release(&mesh->coords);
  darray_uint32_release(&mesh->indices);
  MEM_RM(allocator, mesh);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
scpr_mesh_create
  (struct scpr_device* dev,
   struct scpr_mesh** out_mesh)
{
  struct scpr_mesh* mesh = NULL;
  res_T res = RES_OK;

  if(!dev || !out_mesh) {
    res = RES_BAD_ARG;
    goto error;
  }

  mesh = (struct scpr_mesh*)
    MEM_CALLOC(dev->allocator, 1, sizeof(struct scpr_mesh));
  if(!mesh) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&mesh->ref);
  mesh->device = dev;
  SCPR(device_ref_get(dev));
  darray_int64_init(dev->allocator, &mesh->coords);
  darray_uint32_init(dev->allocator, &mesh->indices);

exit:
  if(out_mesh) *out_mesh = mesh;
  return res;

error:
  if(mesh) {
    SCPR(mesh_ref_put(mesh));
    mesh = NULL;
  }
  goto exit;
}

res_T
scpr_mesh_ref_get(struct scpr_mesh* mesh)
{
  if(!mesh) return RES_BAD_ARG;
  ref_get(&mesh->ref);
  return RES_OK;
}

res_T
scpr_mesh_ref_put(struct scpr_mesh* mesh)
{
  if(!mesh) return RES_BAD_ARG;
  ref_put(&mesh->ref, mesh_release);
  return RES_OK;
}

res_T
scpr_mesh_setup_indexed_vertices
  (struct scpr_mesh* mesh,
   const size_t ntris,
   void (*get_indices)(const size_t itri, size_t ids[3], void* ctx),
   const size_t nverts,
   void (*get_position)(const size_t ivert, double pos[2], void* ctx),
   void* data)
{
  int64_t* pos = NULL;
  uint32_t* ids = NULL;
  size_t i, j;
  res_T res = RES_OK;

  if(!mesh || !ntris || !get_indices || !nverts || !get_position || !data) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(ntris > UINT32_MAX) {
    logger_print(mesh->device->logger, LOG_ERROR,
        "Too many triangles.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  if(nverts > UINT32_MAX) {
    logger_print(mesh->device->logger, LOG_ERROR,
        "Too many vertices.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  ERR(darray_int64_resize(&mesh->coords, nverts*2/*#coords per vertex*/));
  ERR(darray_uint32_resize(&mesh->indices, ntris*3/*#vertices per triangle*/));

  /* Fetch mesh positions */
  pos = darray_int64_data_get(&mesh->coords);
  FOR_EACH(i, 0, nverts) {
    double posd[2];
    get_position(i, posd, data);
    ERR(scpr_device_scale(mesh->device, posd, 2, pos + i*2));
  }

  /* Fetch mesh indices */
  ids = darray_uint32_data_get(&mesh->indices);
  FOR_EACH(i, 0, ntris) {
    size_t tmp[3];
    get_indices(i, tmp, data);
    if(tmp[0] >= nverts || tmp[1] >= nverts || tmp[2] >= nverts) {
      res = RES_BAD_ARG;
      goto error;
    }
    for(j = 0; j < 3; j++) ids[i*3 + j] = (uint32_t)tmp[j];
  }

exit:
  return res;
error:
  if(mesh) {
    darray_int64_clear(&mesh->coords);
    darray_uint32_clear(&mesh->indices);
  }
  goto exit;
}

res_T
scpr_mesh_get_vertices_count(const struct scpr_mesh* mesh, size_t* nverts)
{
  if(!mesh || !nverts) return RES_BAD_ARG;
  ASSERT((darray_int64_size_get(&mesh->coords) % 2/*#coords per vertex*/)==0);
  *nverts = darray_int64_size_get(&mesh->coords) / 2/*#coords per vertex*/;
  return RES_OK;
}

res_T
scpr_mesh_get_triangles_count(const struct scpr_mesh* mesh, size_t* ntris)
{
  if(!mesh || !ntris) return RES_BAD_ARG;
  ASSERT((darray_uint32_size_get(&mesh->indices)%3/*#vertices per triangle*/)==0);
  *ntris = darray_uint32_size_get(&mesh->indices) / 3/*#vertices per triangle*/;
  return RES_OK;
}

res_T
scpr_mesh_get_indices
  (const struct scpr_mesh* mesh, const size_t itri, size_t ids[3])
{
  size_t ntris;
  const size_t i = itri * 3/*#vertices per triangle*/;
  if(!mesh || !ids) return RES_BAD_ARG;
  SCPR(mesh_get_triangles_count(mesh, &ntris));
  if(itri >= ntris) return RES_BAD_ARG;
  ids[0] = darray_uint32_cdata_get(&mesh->indices)[i+0];
  ids[1] = darray_uint32_cdata_get(&mesh->indices)[i+1];
  ids[2] = darray_uint32_cdata_get(&mesh->indices)[i+2];
  return RES_OK;
}

res_T
scpr_mesh_get_position
  (const struct scpr_mesh* mesh, const size_t ivert, double pos[2])
{
  size_t nverts;
  int64_t pos64[2];
  const size_t i = ivert * 2/*#coords per vertex*/;
  if(!mesh || !pos) return RES_BAD_ARG;
  SCPR(mesh_get_vertices_count(mesh, &nverts));
  if(ivert >= nverts) return RES_BAD_ARG;
  pos64[0] = darray_int64_cdata_get(&mesh->coords)[i+0];
  pos64[1] = darray_int64_cdata_get(&mesh->coords)[i+1];
  SCPR(device_unscale(mesh->device, pos64, 2, pos));
  return RES_OK;
}

res_T
scpr_mesh_clip
  (struct scpr_mesh* mesh,
   const enum scpr_operation op,
   struct scpr_polygon* poly_desc)
{
  int64_t lower[2], upper[2];
  struct polygon* polygon = NULL; /* Use to triangulate clipped polygons */
  struct darray_int64 coords; /* Coordinates of the clipped mesh */
  struct darray_uint32 indices; /* Indices of the clipped mesh */
  struct htable_vertex vertices; /* Map a coordinate to its index */
  Clipper2Lib::Clipper64 clipper;
  Clipper2Lib::Paths64 output; /* Contour of the clipped polgyon */
  Clipper2Lib::Paths64 cand_path; /* Contour of the candidate polygon */
  Clipper2Lib::Path64 tmp;
  Clipper2Lib::ClipType clip_type; /* Type of clipping to perform */
  size_t itri, ntris, ivert;
  struct mem_allocator* allocator;
  int i;

  res_T res = RES_OK;

  if(!mesh || !poly_desc || (unsigned)op >= SCPR_OPERATIONS_COUNT__)
    return RES_BAD_ARG;

  allocator = mesh->device->allocator;
  clip_type = scpr_operation_to_clip_type(op);

  darray_int64_init(allocator, &coords);
  darray_uint32_init(allocator, &indices);
  htable_vertex_init(allocator, &vertices);

  if(aabb_is_degenerated(poly_desc->lower, poly_desc->upper)) goto exit;

  mesh_compute_aabb(mesh, lower, upper);
  if(aabb_is_degenerated(lower, upper)) goto exit;

  /* Compute the overall aabb of the candidate and the clip polygon */
  for(i = 0; i < 2; i++) {
    if(lower[i] > poly_desc->lower[i]) lower[i] = poly_desc->lower[i];
    if(upper[i] < poly_desc->upper[i]) upper[i] = poly_desc->upper[i];
  }

  /* Create the polygon structure used to triangulate the clipped polygons */
  ERR(polygon_create(allocator, &polygon));

  /* Clip the triangles of the mesh */
  SCPR(mesh_get_triangles_count(mesh, &ntris));
  FOR_EACH(itri, 0, ntris) {
    size_t ids[3];
    double tri[3][2];
    int64_t tri64[3][2];

    /* Fetch the triangle vertices and compute its AABB */
    SCPR(mesh_get_indices(mesh, itri, ids));
    SCPR(mesh_get_position(mesh, ids[0], tri[0]));
    SCPR(mesh_get_position(mesh, ids[1], tri[1]));
    SCPR(mesh_get_position(mesh, ids[2], tri[2]));
    SCPR(device_scale(mesh->device, tri[0], 2, tri64[0]));
    SCPR(device_scale(mesh->device, tri[1], 2, tri64[1]));
    SCPR(device_scale(mesh->device, tri[2], 2, tri64[2]));
    triangle_compute_aabb(tri64, lower, upper);

    /* Do not clip triangles that do not intersect the clip AABB */
    if(!aabb_intersect(lower, upper, poly_desc->lower, poly_desc->upper)) {
      if(op != SCPR_AND) {
        ERR(register_triangle(tri64, &coords, &indices, &vertices));
      }
      continue;
    }

    /* Setup the candidate path */
    cand_path.clear();
    tmp.clear();
    FOR_EACH(ivert, 0, 3) {
      Clipper2Lib::Point64 pt;
      pt.x = tri64[ivert][0];
      pt.y = tri64[ivert][1];
      tmp.push_back(pt);
    }
    cand_path.push_back(tmp);

    /* Clip the polygon */
    clipper.Clear();
    clipper.AddSubject(cand_path);
    clipper.AddClip(poly_desc->paths);
    clipper.Execute(clip_type, Clipper2Lib::FillRule::EvenOdd, output);

    /* Register the resulting clipped polygons */
    ERR(register_paths(mesh->device, output, &coords, &indices, &vertices, polygon));
  }

  ERR(darray_int64_copy_and_clear(&mesh->coords, &coords));
  ERR(darray_uint32_copy_and_clear(&mesh->indices, &indices));

exit:
  if(polygon) POLYGON(ref_put(polygon));
  darray_int64_release(&coords);
  darray_uint32_release(&indices);
  htable_vertex_release(&vertices);
  return res;
error:
  goto exit;
}

