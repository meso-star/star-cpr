/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

int
main(int argc, char** argv)
{
  const double coords[] = {
    0.0, 0.0,
    0.0, 0.5,
    0.0, 1.0,
    0.5, 0.0,
    0.5, 0.5,
    0.5, 1.0,
    1.0, 0.0,
    1.0, 0.5,
    1.0, 1.0
  };
  const size_t nverts = sizeof(coords)/(2*sizeof(double));
  const size_t indices[] = {
    0, 1, 3,
    3, 1, 4,
    1, 2, 4,
    4, 2, 5,
    3, 4, 6,
    6, 4, 7,
    4, 5, 7,
    7, 5, 8
  };
  const size_t ntris = sizeof(indices)/(3*sizeof(size_t));
  const size_t indices_bad[] = { 7, 5, 9 };
  size_t ids[3];
  double pos[2];
  size_t i, n;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  struct mem_allocator allocator;
  struct mesh_context ctx;
  struct scpr_mesh* mesh;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  args.allocator = &allocator;
  OK(scpr_device_create(&args, &dev));

  BAD(scpr_mesh_create(NULL, NULL));
  BAD(scpr_mesh_create(dev, NULL));
  OK(scpr_mesh_create(dev, &mesh));

  BAD(scpr_mesh_ref_get(NULL));
  OK(scpr_mesh_ref_get(mesh));
  BAD(scpr_mesh_ref_put(NULL));
  OK(scpr_mesh_ref_put(mesh));
  OK(scpr_mesh_ref_put(mesh));

  OK(scpr_mesh_create(dev, &mesh));

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.indices = indices;
  ctx.ntris = ntris;

  #define SETUP scpr_mesh_setup_indexed_vertices
  BAD(SETUP(NULL, 0, NULL, 0, NULL, NULL));
  BAD(SETUP(mesh, 0, NULL, 0, NULL, NULL));
  BAD(SETUP(NULL, ntris, NULL, 0, NULL, NULL));
  BAD(SETUP(mesh, ntris, NULL, 0, NULL, NULL));
  BAD(SETUP(NULL, 0, mget_ids, 0, NULL, NULL));
  BAD(SETUP(mesh, 0, mget_ids, 0, NULL, NULL));
  BAD(SETUP(NULL, ntris, mget_ids, 0, NULL, NULL));
  BAD(SETUP(mesh, ntris, mget_ids, 0, NULL, NULL));
  BAD(SETUP(NULL, 0, NULL, nverts, NULL, NULL));
  BAD(SETUP(mesh, 0, NULL, nverts, NULL, NULL));
  BAD(SETUP(NULL, ntris, NULL, nverts, NULL, NULL));
  BAD(SETUP(mesh, ntris, NULL, nverts, NULL, NULL));
  BAD(SETUP(NULL, 0, mget_ids, nverts, NULL, NULL));
  BAD(SETUP(mesh, 0, mget_ids, nverts, NULL, NULL));
  BAD(SETUP(NULL, ntris, mget_ids, nverts, NULL, NULL));
  BAD(SETUP(mesh, ntris, mget_ids, nverts, NULL, NULL));
  BAD(SETUP(NULL, 0, NULL, 0, mget_pos, NULL));
  BAD(SETUP(mesh, 0, NULL, 0, mget_pos, NULL));
  BAD(SETUP(NULL, ntris, NULL, 0, mget_pos, NULL));
  BAD(SETUP(mesh, ntris, NULL, 0, mget_pos, NULL));
  BAD(SETUP(NULL, 0, mget_ids, 0, mget_pos, NULL));
  BAD(SETUP(mesh, 0, mget_ids, 0, mget_pos, NULL));
  BAD(SETUP(NULL, ntris, mget_ids, 0, mget_pos, NULL));
  BAD(SETUP(mesh, ntris, mget_ids, 0, mget_pos, NULL));
  BAD(SETUP(NULL, 0, NULL, nverts, mget_pos, NULL));
  BAD(SETUP(mesh, 0, NULL, nverts, mget_pos, NULL));
  BAD(SETUP(NULL, ntris, NULL, nverts, mget_pos, NULL));
  BAD(SETUP(mesh, ntris, NULL, nverts, mget_pos, NULL));
  BAD(SETUP(NULL, 0, mget_ids, nverts, mget_pos, NULL));
  BAD(SETUP(mesh, 0, mget_ids, nverts, mget_pos, NULL));
  BAD(SETUP(NULL, ntris, mget_ids, nverts, mget_pos, NULL));
  BAD(SETUP(mesh, ntris, mget_ids, nverts, mget_pos, NULL));
  BAD(SETUP(NULL, 0, NULL, 0, NULL, &ctx));
  BAD(SETUP(mesh, 0, NULL, 0, NULL, &ctx));
  BAD(SETUP(NULL, ntris, NULL, 0, NULL, &ctx));
  BAD(SETUP(mesh, ntris, NULL, 0, NULL, &ctx));
  BAD(SETUP(NULL, 0, mget_ids, 0, NULL, &ctx));
  BAD(SETUP(mesh, 0, mget_ids, 0, NULL, &ctx));
  BAD(SETUP(NULL, ntris, mget_ids, 0, NULL, &ctx));
  BAD(SETUP(mesh, ntris, mget_ids, 0, NULL, &ctx));
  BAD(SETUP(NULL, 0, NULL, nverts, NULL, &ctx));
  BAD(SETUP(mesh, 0, NULL, nverts, NULL, &ctx));
  BAD(SETUP(NULL, ntris, NULL, nverts, NULL, &ctx));
  BAD(SETUP(mesh, ntris, NULL, nverts, NULL, &ctx));
  BAD(SETUP(NULL, 0, mget_ids, nverts, NULL, &ctx));
  BAD(SETUP(mesh, 0, mget_ids, nverts, NULL, &ctx));
  BAD(SETUP(NULL, ntris, mget_ids, nverts, NULL, &ctx));
  BAD(SETUP(mesh, ntris, mget_ids, nverts, NULL, &ctx));
  BAD(SETUP(NULL, 0, NULL, 0, mget_pos, &ctx));
  BAD(SETUP(mesh, 0, NULL, 0, mget_pos, &ctx));
  BAD(SETUP(NULL, ntris, NULL, 0, mget_pos, &ctx));
  BAD(SETUP(mesh, ntris, NULL, 0, mget_pos, &ctx));
  BAD(SETUP(NULL, 0, mget_ids, 0, mget_pos, &ctx));
  BAD(SETUP(mesh, 0, mget_ids, 0, mget_pos, &ctx));
  BAD(SETUP(NULL, ntris, mget_ids, 0, mget_pos, &ctx));
  BAD(SETUP(mesh, ntris, mget_ids, 0, mget_pos, &ctx));
  BAD(SETUP(NULL, 0, NULL, nverts, mget_pos, &ctx));
  BAD(SETUP(mesh, 0, NULL, nverts, mget_pos, &ctx));
  BAD(SETUP(NULL, ntris, NULL, nverts, mget_pos, &ctx));
  BAD(SETUP(mesh, ntris, NULL, nverts, mget_pos, &ctx));
  BAD(SETUP(NULL, 0, mget_ids, nverts, mget_pos, &ctx));
  BAD(SETUP(mesh, 0, mget_ids, nverts, mget_pos, &ctx));
  BAD(SETUP(NULL, ntris, mget_ids, nverts, mget_pos, &ctx));
  OK(SETUP(mesh, ntris, mget_ids, nverts, mget_pos, &ctx));
  ctx.indices = indices_bad;
  BAD(SETUP(mesh, 1, mget_ids, nverts, mget_pos, &ctx));
  #undef SETUP

  BAD(scpr_mesh_get_triangles_count(NULL, NULL));
  BAD(scpr_mesh_get_triangles_count(mesh, NULL));
  BAD(scpr_mesh_get_triangles_count(NULL, &n));
  OK(scpr_mesh_get_triangles_count(mesh, &n));
  CHK(n == 0);

  BAD(scpr_mesh_get_vertices_count(NULL, NULL));
  BAD(scpr_mesh_get_vertices_count(mesh, NULL));
  BAD(scpr_mesh_get_vertices_count(NULL, &n));
  OK(scpr_mesh_get_vertices_count(mesh, &n));
  CHK(n == 0);

  ctx.indices = indices;
  OK(scpr_mesh_setup_indexed_vertices(mesh, ntris, mget_ids, nverts, mget_pos, &ctx));
  OK(scpr_mesh_get_triangles_count(mesh, &n));
  CHK(n == ntris);
  OK(scpr_mesh_get_vertices_count(mesh, &n));
  CHK(n == nverts);

  BAD(scpr_mesh_get_indices(NULL, ntris, NULL));
  BAD(scpr_mesh_get_indices(mesh, ntris, NULL));
  BAD(scpr_mesh_get_indices(NULL, 0, NULL));
  BAD(scpr_mesh_get_indices(mesh, 0, NULL));
  BAD(scpr_mesh_get_indices(NULL, ntris, ids));
  BAD(scpr_mesh_get_indices(mesh, ntris, ids));
  BAD(scpr_mesh_get_indices(NULL, 0, ids));
  FOR_EACH(i, 0, ntris) {
    OK(scpr_mesh_get_indices(mesh, i, ids));
    CHK(ids[0] == indices[i*3+0]);
    CHK(ids[1] == indices[i*3+1]);
    CHK(ids[2] == indices[i*3+2]);
  }

  BAD(scpr_mesh_get_position(NULL, nverts, NULL));
  BAD(scpr_mesh_get_position(mesh, nverts, NULL));
  BAD(scpr_mesh_get_position(NULL, 0, NULL));
  BAD(scpr_mesh_get_position(mesh, 0, NULL));
  BAD(scpr_mesh_get_position(NULL, nverts, pos));
  BAD(scpr_mesh_get_position(mesh, nverts, pos));
  CHK(scpr_mesh_get_position(NULL, 0, pos));
  FOR_EACH(i, 0, nverts) {
    OK(scpr_mesh_get_position(mesh, i, pos));
    CHK(pos[0] == coords[i*2+0]);
    CHK(pos[1] == coords[i*2+1]);
  }

  OK(scpr_mesh_ref_put(mesh));
  OK(scpr_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

