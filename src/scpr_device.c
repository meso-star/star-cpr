/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "scpr.h"
#include "scpr_c.h"

#include <polygon.h>
#include <rsys/mem_allocator.h>
#include <rsys/logger.h>
#include <rsys/rsys.h>
#include <rsys/double2.h>

#include <math.h>

#ifdef OS_UNIX
  /* On UNIX assume a VT100-like terminal emulator */
  #define MSG_INFO_PREFIX "star-cpr (\x1b[1m\x1b[32minfo\x1b[0m): "
  #define MSG_ERROR_PREFIX "star-cpr (\x1b[1m\x1b[31merror\x1b[0m): "
  #define MSG_WARNING_PREFIX "star-cpr (\x1b[1m\x1b[33mwarning\x1b[0m): "
#else
  #define MSG_INFO_PREFIX "star-cpr (info): "
  #define MSG_ERROR_PREFIX "star-cpr (error): "
  #define MSG_WARNING_PREFIX "star-cpr (warning): "
#endif

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
device_release(ref_T* ref)
{
  struct scpr_device* device;
  struct mem_allocator* allocator;
  ASSERT(ref);
  device = CONTAINER_OF(ref, struct scpr_device, ref);
  allocator = device->allocator;
  MEM_RM(allocator, device);
}

int INLINE
is_in_range
  (const double x,
   const double range[2])
{
  ASSERT(range);
  return range[0] <= x && x <= range[1];
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
scpr_device_create
  (const struct scpr_device_create_args* args,
   struct scpr_device** out_device)
{
  struct scpr_device* device = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!args || !out_device || args->precision < 0 || args->precision > 8) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  device = (struct scpr_device*)MEM_CALLOC(allocator, 1, sizeof(struct scpr_device));
  if(!device) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&device->ref);
  device->allocator = allocator;
  device->logger = args->logger ? args->logger : LOGGER_DEFAULT;
  device->precision = args->precision;
  /* to optimize scaling / descaling precision
   * set the scale to a power of double's radix (2) (#25) */
  device->scale = pow(2, (int)log2(pow(10, device->precision)) + 1);
  device->inv_scale = 1/device->scale;
  device->range[1] = pow(2, 46 - device->precision);
  device->range[0] = -device->range[1];

exit:
  if(out_device) *out_device = device;
  return res;

error:
  if(device) {
    SCPR(device_ref_put(device));
    device = NULL;
  }
  goto exit;
}

res_T
scpr_device_ref_get
  (struct scpr_device* device)
{
  if(!device) return RES_BAD_ARG;
  ref_get(&device->ref);
  return RES_OK;
}

res_T
scpr_device_ref_put
  (struct scpr_device* device)
{
  if(!device) return RES_BAD_ARG;
  ref_put(&device->ref, device_release);
  return RES_OK;
}

res_T
scpr_device_get_range
  (const struct scpr_device* dev,
   double range[2])
{
  if(!dev || !range) return RES_BAD_ARG;
  d2_set(range, dev->range);
  return RES_OK;
}

res_T
scpr_device_in_range
  (const struct scpr_device* dev,
   const double* values,
   const size_t count,
   int* in_range)

{
  size_t i;
  int in = 1;
  if(!dev || !values || !in_range) return RES_BAD_ARG;
  for(i = 0; i < count; i++) {
   if(!is_in_range(values[i], dev->range)) {
     in = 0;
     break;
   }
  }
  *in_range = in;
  return RES_OK;
}

res_T
scpr_device_scale
  (const struct scpr_device* dev,
   const double* values,
   const size_t count,
   int64_t* scaled)
{
  size_t i;
  if(!dev || !values || !scaled) return RES_BAD_ARG;
  for(i = 0; i < count; i++) {
   if(!is_in_range(values[i], dev->range)) return RES_BAD_ARG;
    scaled[i] = std::llround(values[i] * dev->scale);
  }
  return RES_OK;
}

res_T
scpr_device_unscale
  (const struct scpr_device* dev,
   const int64_t* values,
   const size_t count,
   double* unscaled)
{
  size_t i;
  if(!dev || !values || !unscaled) return RES_BAD_ARG;
  for(i = 0; i < count; i++) {
    unscaled[i] = (double)values[i] * dev->inv_scale;
  }
  return RES_OK;
}
