/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <rsys/rsys.h>

#include <memory.h>

struct cpt {
  size_t simple, overlapping;
};

static int
simple_intersection
  (struct scpr_callback_segment* segment1,
   struct scpr_callback_segment* segment2,
   void* data)
{
  struct cpt* cpt = (struct cpt*)data;
  (void)segment1; (void)segment2;
  ASSERT(cpt);
  cpt->simple++;
  return 0;
}

static int
overlapping_segments
  (struct scpr_callback_segment* segment1,
   struct scpr_callback_segment* segment2,
   void* data)
{
  struct cpt* cpt = (struct cpt*)data;
  (void)segment1; (void)segment2;
  ASSERT(cpt);
  cpt->overlapping++;
  return 0;
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct scpr_device* dev;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_intersector* inter;
  struct scpr_intersector_check_callbacks cb
    = SCPR_INTERSECTOR_CHECK_CALLBACKS_NULL__ ;
  struct polygon_context ctx;
  struct scpr_polygon *p01, *p23, *p4;
  size_t ncomps = 2;
  double** coords;
  /*                         1,1
   *            +-----------+----+
   *            |         / |   /
   *            |       /   | /
   *      +-----------+-C2--+
   *      |     |     |     |
   *      |     |     |     |
   *      |     +-C0--+-+---+----+----+
   *      |   0,0     |/         |    |
   *      |           +-C3-------+-C4-+
   *      +-C1--------+
   * -0.5,-0.5
   */
  double coords0[] = {
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0
  };
  double coords1[] = {
    -0.5, -0.5,
    0.5, -0.5,
    0.5, 0.5,
    -0.5, 0.5
  };
  double coords2[] = {
    0.5, 0.5,
    1.0, 0.5,
    1.5, 1.0,
    1.0, 1.0
  };
  double coords3[] = {
    0.5, -0.3,
    1.5, -0.3,
    1.5, 0.0,
    0.7, 0.0
  };
  double coords4[] = {
    1.5, -0.3,
    2.0, -0.3,
    2.0, 0.0,
    1.5, 0
  };
  size_t nverts[] = { 4, 4 };
  struct cpt cpt = { 0, 0 };
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  args.allocator = &allocator;
  args.verbosity_level = 3;
  args.precision = 6;
  OK(scpr_device_create(&args, &dev));

  BAD(scpr_intersector_create(NULL, NULL));
  BAD(scpr_intersector_create(dev, NULL));
  BAD(scpr_intersector_create(NULL, &inter));
  OK(scpr_intersector_create(dev, &inter));

  BAD(scpr_intersector_ref_get(NULL));
  OK(scpr_intersector_ref_get(inter));

  BAD(scpr_intersector_ref_put(NULL));
  OK(scpr_intersector_ref_put(inter));

  OK(scpr_polygon_create(dev, &p01));
  OK(scpr_polygon_create(dev, &p23));
  OK(scpr_polygon_create(dev, &p4));
  coords = (double**)MEM_CALLOC(&allocator, ncomps, sizeof(*coords));
  coords[0] = (double*)MEM_CALLOC(&allocator, nverts[0], 2*sizeof(**coords));
  coords[1] = (double*)MEM_CALLOC(&allocator, nverts[1], 2*sizeof(**coords));

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.ncomps = ncomps;

  #define SETUP scpr_polygon_setup_indexed_vertices
  memcpy(coords[0], coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(coords[1], coords1, 2*nverts[1]*sizeof(**coords));
  OK(SETUP(p01, ncomps, pget_nverts, pget_pos, &ctx));

  memcpy(coords[0], coords2, 2*nverts[0]*sizeof(**coords));
  memcpy(coords[1], coords3, 2*nverts[1]*sizeof(**coords));
  OK(SETUP(p23, ncomps, pget_nverts, pget_pos, &ctx));

  memcpy(coords[0], coords4, 2*nverts[0]*sizeof(**coords));
  OK(SETUP(p4, 1, pget_nverts, pget_pos, &ctx));

  BAD(scpr_intersector_register_component(NULL, NULL, ncomps));
  BAD(scpr_intersector_register_component(NULL, NULL, 0));
  BAD(scpr_intersector_register_component(NULL, p01, ncomps));
  BAD(scpr_intersector_register_component(inter, NULL, ncomps));
  BAD(scpr_intersector_register_component(NULL, p01, 0));
  BAD(scpr_intersector_register_component(inter, NULL, 0));
  BAD(scpr_intersector_register_component(inter, p01, ncomps));
  OK(scpr_intersector_register_component(inter, p01, 0));
  OK(scpr_intersector_register_component(inter, p01, 1));
  OK(scpr_intersector_register_component(inter, p23, 0));
  OK(scpr_intersector_register_component(inter, p23, 1));
  BAD(scpr_intersector_register_component(inter, p23, 1)); /* Registered twice */

  BAD(scpr_intersector_register_polygon(NULL, NULL));
  BAD(scpr_intersector_register_polygon(NULL, p01));
  BAD(scpr_intersector_register_polygon(inter, NULL));
  OK(scpr_intersector_register_polygon(inter, p4));
  BAD(scpr_intersector_register_polygon(inter, p4)); /* Registered twice */

  BAD(scpr_intersector_check(NULL, NULL, NULL));
  BAD(scpr_intersector_check(NULL, &cb, NULL));
  BAD(scpr_intersector_check(inter, NULL, NULL));
  BAD(scpr_intersector_check(inter, &cb, NULL));
  BAD(scpr_intersector_check(inter, &cb, &cpt)); /* Callbacks are all NULL */
  /* Report only intersections */
  cb.simple_intersection = simple_intersection;
  OK(scpr_intersector_check(inter, &cb, &cpt));
  CHK(cpt.simple == 2);
  CHK(cpt.overlapping == 0);
  /* Report intersections and overlapping */
  cb.overlapping_segments = overlapping_segments;
  cpt.simple = cpt.overlapping = 0;
  OK(scpr_intersector_check(inter, &cb, &cpt));
  CHK(cpt.simple == 2);
  CHK(cpt.overlapping == 2);

  OK(scpr_polygon_ref_put(p01));
  OK(scpr_polygon_ref_put(p23));
  OK(scpr_polygon_ref_put(p4));
  OK(scpr_intersector_ref_put(inter));
  OK(scpr_device_ref_put(dev));

  MEM_RM(&allocator, coords[0]);
  MEM_RM(&allocator, coords[1]);
  MEM_RM(&allocator, coords);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

