/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <rsys/rsys.h>
#include <rsys/mem_allocator.h>

#include <memory.h>


int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  double c0[] = {0, 5, 8, 5, 8, 9, 0, 9 };
  double c1[] = {1, 1, 3, 1, 3, 9, 1, 9 };
  double c2[] = {3, 6, 5, 6, 5, 8, 4, 7, 3, 8 };
  size_t n0[] = { 4 };
  size_t n1[] = { 4 };
  size_t n2[] = { 5 };
  struct scpr_polygon* p0 = NULL;
  struct scpr_polygon* p1 = NULL;
  struct scpr_polygon* p2 = NULL;
  struct polygon_context context;
  double** pos;
  int in;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  pos = (double**)MEM_CALLOC(&allocator, 1, sizeof(*pos));
  *pos = (double*)MEM_CALLOC(&allocator, 10, sizeof(**pos));

  args.allocator = &allocator;
  args.precision = 2;
  OK(scpr_device_create(&args, &dev));

  OK(scpr_polygon_create(dev, &p0));
  OK(scpr_polygon_create(dev, &p1));
  OK(scpr_polygon_create(dev, &p2));

  context.ncomps = 1;
  context.coords = pos;

  context.nverts = n0;
  memcpy(*pos, c0, 2*n0[0]*sizeof(**pos));
  OK(scpr_polygon_setup_indexed_vertices(p0, 1, pget_nverts, pget_pos, &context));

  context.nverts = n1;
  memcpy(*pos, c1, 2*n1[0]*sizeof(**pos));
  OK(scpr_polygon_setup_indexed_vertices(p1, 1, pget_nverts, pget_pos, &context));

  context.nverts = n2;
  memcpy(*pos, c2, 2*n2[0]*sizeof(**pos));
  OK(scpr_polygon_setup_indexed_vertices(p2, 1, pget_nverts, pget_pos, &context));

  BAD(scpr_is_component_in_component(NULL, 0, NULL, 0, NULL));
  OK(scpr_is_component_in_component(p1, 0, p2, 0, &in));
  CHK(in == 0);
  OK(scpr_is_component_in_component(p2, 0, p1, 0, &in));
  CHK(in == 0);
  OK(scpr_is_component_in_component(p2, 0, p0, 0, &in));
  CHK(in == 1);
  OK(scpr_is_component_in_component(p0, 0, p2, 0, &in));
  CHK(in == 0);

  OK(scpr_device_ref_put(dev));
  OK(scpr_polygon_ref_put(p0));
  OK(scpr_polygon_ref_put(p1));
  OK(scpr_polygon_ref_put(p2));

  MEM_RM(&allocator, *pos);
  MEM_RM(&allocator, pos);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
