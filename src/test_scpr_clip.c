/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <rsys/math.h>
#include <rsys/stretchy_array.h>

#include <memory.h>

static void
dump_obj(FILE* stream, const struct scpr_mesh* mesh)
{
  size_t i, n;

  CHK(stream != NULL);
  CHK(mesh != NULL);

  OK(scpr_mesh_get_vertices_count(mesh, &n));
  FOR_EACH(i, 0, n) {
    double pos[2];
    OK(scpr_mesh_get_position(mesh, i, pos));
    fprintf(stream, "v %g %g 0\n", SPLIT2(pos));
  }

  OK(scpr_mesh_get_triangles_count(mesh, &n));
  FOR_EACH(i, 0, n) {
    size_t ids[3];
    OK(scpr_mesh_get_indices(mesh, i, ids));
    fprintf(stream, "f %lu %lu %lu\n",
      (unsigned long)(ids[0] + 1),
      (unsigned long)(ids[1] + 1),
      (unsigned long)(ids[2] + 1));
  }
}

static void
test_triangle
  (struct scpr_device* dev,
   struct mem_allocator* allocator,
   struct scpr_mesh* mesh)
{
  const double triangle_pos1[] = { 0, 0, 0, 1, 1, 0 };
  double triangle_pos2[] = { 0, 0, 0, 1e20, 1e20, 0}; /* To be replaced */
  const size_t triangle_ids[] = { 0, 1, 2 };
  double** clip_pos;
  double range[2];
  size_t nverts[] = { 3 };
  size_t ncomps = 1;
  const double clip_pos0[] = { -1.0, 0.25, 1.0, 0.75, 1, 0.25 };
  struct scpr_polygon* poly;
  struct polygon_context pctx;
  struct mesh_context mctx;
  size_t ntris;

  /* Set out-of-range value in triangle_pos2 */
  SCPR(device_get_range(dev, range));
  triangle_pos2[3] = triangle_pos2[4] = range[1] + 1;

  clip_pos = (double**)MEM_CALLOC(allocator, ncomps, sizeof(*clip_pos));
  *clip_pos = (double*)MEM_CALLOC(allocator, nverts[0], 2*sizeof(**clip_pos));
  memcpy(*clip_pos, clip_pos0, 2*nverts[0]*sizeof(**clip_pos));

  pctx.coords = clip_pos;
  pctx.nverts = nverts;
  pctx.ncomps = ncomps;
  OK(scpr_polygon_create(dev, &poly));
  OK(scpr_polygon_setup_indexed_vertices(poly, ncomps, pget_nverts, pget_pos, &pctx));

  /* Check out-of-range */
  mctx.coords = triangle_pos2;
  mctx.nverts = 3;
  mctx.indices = triangle_ids;
  mctx.ntris = 1;
  BAD(scpr_mesh_setup_indexed_vertices
    (mesh, mctx.ntris, mget_ids, 3, mget_pos, &mctx));

  mctx.coords = triangle_pos1;
  OK(scpr_mesh_setup_indexed_vertices
    (mesh, mctx.ntris, mget_ids, 3, mget_pos, &mctx));

  BAD(scpr_mesh_clip(NULL, SCPR_OPERATIONS_COUNT__, NULL));
  BAD(scpr_mesh_clip(mesh, SCPR_OPERATIONS_COUNT__, NULL));
  BAD(scpr_mesh_clip(NULL, SCPR_OPERATIONS_COUNT__, poly));
  BAD(scpr_mesh_clip(mesh, SCPR_OPERATIONS_COUNT__, poly));
  BAD(scpr_mesh_clip(NULL, SCPR_SUB, NULL));
  BAD(scpr_mesh_clip(mesh, SCPR_SUB, NULL));
  BAD(scpr_mesh_clip(NULL, SCPR_SUB, poly));
  OK(scpr_mesh_clip(mesh, SCPR_SUB, poly));

  /*dump_obj(stdout, mesh);*/

  OK(scpr_mesh_get_triangles_count(mesh, &ntris));
  CHK(ntris == 3);

  MEM_RM(allocator, *clip_pos);
  MEM_RM(allocator, clip_pos);
  OK(scpr_polygon_ref_put(poly));
}

static void
test_quad
  (struct scpr_device* dev,
   struct mem_allocator* allocator,
   struct scpr_mesh* mesh)
{
  const double quad_pos[] = { 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0 };
  const size_t quad_ids[] = { 0, 1, 3, 3, 1, 2 };
  double** clip_pos;
  size_t nverts[] = { 4 };
  size_t ncomps = 1;
  const double clip_pos0[] = { -0.25, 0.25, -0.25, 0.75, 1.25, 0.75, 1.25, 0.25 };
  struct scpr_polygon* poly;
  struct polygon_context pctx;
  struct mesh_context mctx;

  clip_pos = (double**)MEM_CALLOC(allocator, ncomps, sizeof(*clip_pos));
  *clip_pos = (double*)MEM_CALLOC(allocator, nverts[0], 2*sizeof(**clip_pos));
  memcpy(*clip_pos, clip_pos0, 2*nverts[0]*sizeof(**clip_pos));

  pctx.coords = clip_pos;
  pctx.nverts = nverts;
  pctx.ncomps = ncomps;
  OK(scpr_polygon_create(dev, &poly));
  OK(scpr_polygon_setup_indexed_vertices(poly, ncomps, pget_nverts, pget_pos, &pctx));

  mctx.coords = quad_pos;
  mctx.nverts = sizeof(quad_pos)/(2*sizeof(double));
  mctx.indices = quad_ids;
  mctx.ntris = sizeof(quad_ids)/(3*sizeof(size_t));
  OK(scpr_mesh_setup_indexed_vertices
    (mesh, mctx.ntris, mget_ids, mctx.nverts, mget_pos, &mctx));

  OK(scpr_mesh_clip(mesh, SCPR_AND, poly));

  /*dump_obj(stdout, mesh);*/

  MEM_RM(allocator, *clip_pos);
  MEM_RM(allocator, clip_pos);
  OK(scpr_polygon_ref_put(poly));
}

static void
test_disk
  (struct scpr_device* dev,
   struct mem_allocator* allocator,
   struct scpr_mesh* mesh)
{
  double** clip_pos;
  size_t nverts[] = { 4 };
  size_t ncomps = 1;
  const double clip_pos0[]
    =  { -1.75, -1.75, 1.75, -1.75, 1.75, 1.75, -1.75, 1.75 };
  const size_t ninternal_disks = 10;
  const double radius = 2.5;
  const double internal_disk_step = radius / (double)ninternal_disks;
  const size_t nslices = 64;
  struct scpr_polygon* poly;
  struct polygon_context pctx;
  struct mesh_context mctx;
  double* pos = NULL;
  size_t* ids = NULL;
  size_t i, j;

  clip_pos = (double**)MEM_CALLOC(allocator, ncomps, sizeof(*clip_pos));
  *clip_pos = (double*)MEM_CALLOC(allocator, nverts[0], 2*sizeof(**clip_pos));
  memcpy(*clip_pos, clip_pos0, 2*nverts[0]*sizeof(**clip_pos));

  FOR_EACH(i, 0, ninternal_disks) {
    const double r = (double)(i+1)*internal_disk_step;
    FOR_EACH(j, 0, nslices) {
      const double theta = (double)j / (double)nslices * 2 * PI;
      const double x = r * cos(theta);
      const double y = r * sin(theta);
      sa_push(pos, x);
      sa_push(pos, y);
    }
  }

  /* Center point */
  sa_push(pos, 0.0);
  sa_push(pos, 0.0);

  FOR_EACH(i, 0, ninternal_disks-1) {
    const size_t offset = (i+1) * nslices;
    FOR_EACH(j, 0, nslices) {
      const size_t id0 = j + offset;
      const size_t id1 = ((j + 1) % nslices) + offset;
      const size_t id2 = id0 - nslices;
      const size_t id3 = id1 - nslices;

      sa_push(ids, id0);
      sa_push(ids, id2);
      sa_push(ids, id1);

      sa_push(ids, id1);
      sa_push(ids, id2);
      sa_push(ids, id3);
    }
  }

  /* Center triangles */
  FOR_EACH(j, 0, nslices) {
    const size_t id0 = j;
    const size_t id1 = (j + 1) % nslices;
    const size_t id2 = sa_size(pos)/2 - 1;

    sa_push(ids, id0);
    sa_push(ids, id2);
    sa_push(ids, id1);
  }

  pctx.coords = clip_pos;
  pctx.nverts = nverts;
  pctx.ncomps = ncomps;
  OK(scpr_polygon_create(dev, &poly));
  OK(scpr_polygon_setup_indexed_vertices(poly, ncomps, pget_nverts, pget_pos, &pctx));

  mctx.coords = pos;
  mctx.nverts = sa_size(pos)/2;
  mctx.indices = ids;
  mctx.ntris = sa_size(ids)/3;
  OK(scpr_mesh_setup_indexed_vertices
    (mesh, mctx.ntris, mget_ids, mctx.nverts, mget_pos, &mctx));

  OK(scpr_mesh_clip(mesh, SCPR_SUB, poly));

  dump_obj(stdout, mesh);

  MEM_RM(allocator, *clip_pos);
  MEM_RM(allocator, clip_pos);
  sa_release(pos);
  sa_release(ids);
  OK(scpr_polygon_ref_put(poly));
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  struct scpr_mesh* mesh;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  args.allocator = &allocator;
  OK(scpr_device_create(&args, &dev));
  OK(scpr_mesh_create(dev, &mesh));

  test_triangle(dev, &allocator, mesh);
  test_quad(dev, &allocator, mesh);
  test_disk(dev, &allocator, mesh);

  OK(scpr_mesh_ref_put(mesh));
  OK(scpr_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

