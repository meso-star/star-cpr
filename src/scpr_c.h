/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCPR_C_H__
#define SCPR_C_H__

#include "scpr.h"
#include "scpr_clipper.h"

#include <stddef.h>
#include <rsys/rsys.h>
#include <rsys/ref_count.h>
#include <rsys/double2.h>
#include <rsys/dynamic_array.h>
#include <rsys/hash_table.h>

#define DARRAY_NAME uint32
#define DARRAY_DATA uint32_t
#include <rsys/dynamic_array.h>

#define DARRAY_NAME int64
#define DARRAY_DATA int64_t
#include <rsys/dynamic_array.h>

#define ERR(Expr) { if((res = (Expr)) != RES_OK) goto error; } (void)0

#define TRY(Expr) \
  try { \
    (Expr); \
  } \
  catch(std::bad_alloc &e) { \
    res = RES_MEM_ERR; \
    goto error; \
  } \
  catch(...) { \
    res = RES_UNKNOWN_ERR; \
    goto error; \
  }

struct mem_allocator;

struct vertex { int64_t pos[2]; };
static FINLINE int
vertex_eq(const struct vertex* a, const struct vertex* b)
{ ASSERT(a && b); return (a->pos[0] == b->pos[0]) && (a->pos[1] == b->pos[1]); }

/* Define the vertex to index hash table */
#define HTABLE_NAME vertex
#define HTABLE_DATA uint32_t
#define HTABLE_KEY struct vertex
#define HTABLE_KEY_FUNCTOR_EQ vertex_eq
#include <rsys/hash_table.h>

struct scpr_device {
  int precision;
  double scale;
  double inv_scale;
  double range[2];

  ref_T ref;
  struct mem_allocator* allocator;
  struct logger* logger;
};

struct scpr_polygon {
  Clipper2Lib::Paths64 paths;
  int64_t lower[2], upper[2]; /* Polygon AABB */

  ref_T ref;
  struct scpr_device* device;
};

struct scpr_mesh {
  struct darray_int64 coords;
  struct darray_uint32 indices;

  ref_T ref;
  struct scpr_device* device;
};

struct intersector_segment {
  uint32_t component, first_vertex;
};
#define DARRAY_NAME segments
#define DARRAY_DATA struct intersector_segment
#include <rsys/dynamic_array.h>

/* A segment htable links a count (1 or 2) to a segment index in the segments
 * darray.
 * The count is the number of ends of the segment at some range_point. */
#define HTABLE_NAME segment_idx
#define HTABLE_KEY uint32_t
#define HTABLE_DATA char
#include <rsys/hash_table.h>

/* Each range_point stores the segments that have 1 or 2 ends there */
struct intersector_range_point {
  struct htable_segment_idx segments;
  int64_t coord;
};
static FINLINE void
range_point_init
  (struct mem_allocator* alloc, struct intersector_range_point* data)
{
  data->coord = INT64_MIN;
  htable_segment_idx_init(alloc, &data->segments);
}
static FINLINE void
range_point_release(struct intersector_range_point* data)
{
  htable_segment_idx_release(&data->segments);
}
static FINLINE res_T
range_point_copy
  (struct intersector_range_point* dst, struct intersector_range_point const* src)
{
  dst->coord = src->coord;
  return htable_segment_idx_copy(&dst->segments, &src->segments);
}
static FINLINE res_T
range_point_copy_and_release
  (struct intersector_range_point* dst, struct intersector_range_point* src)
{
  dst->coord = src->coord;
  return htable_segment_idx_copy_and_release(&dst->segments, &src->segments);
}
/* A darray of range_point. */
#define DARRAY_NAME range_points
#define DARRAY_DATA struct intersector_range_point
#define DARRAY_FUNCTOR_INIT range_point_init
#define DARRAY_FUNCTOR_COPY range_point_copy
#define DARRAY_FUNCTOR_RELEASE range_point_release
#define DARRAY_FUNCTOR_COPY_AND_RELEASE range_point_copy_and_release
#include <rsys/dynamic_array.h>

/* An htable to link a pointer to a range_point to an 1D coordinate */
#define HTABLE_NAME range_point_idx
#define HTABLE_KEY int64_t
#define HTABLE_DATA uint32_t
#include <rsys/hash_table.h>

enum segment_interaction {
  NO_INTERACTION = 0,
  OVERLAP_X = BIT(0),
  CONTACT_X = BIT(1),
  OVERLAP_Y = BIT(2),
  CONTACT_Y = BIT(3),
  SOMETHING_X = OVERLAP_X | CONTACT_X,
  SOMETHING_Y = OVERLAP_Y | CONTACT_Y,
  SOME_OVERLAP = OVERLAP_X | OVERLAP_Y
};
/* A dimension is a structure containing all the points where a segment starts
 * or ends, each one storing a link to all the segments involved there. */
struct intersector_dimension {
  /* Unsorted 1D points registered against this dimension */
  struct htable_range_point_idx unsorted;
  /* Same points, but sorted by increasing coordinates along this dimension */
  struct darray_range_points sorted;
  /* Range on this dimension */
  int64_t range[2];
  /* Flags about segments interaction for this dimension */
  enum segment_interaction overlap, contact;
};

/* An htable to store overlapping information */
struct intersector_segment_pair {
  uint32_t rank1, rank2;
};
static INLINE char
pair_eq
  (struct intersector_segment_pair const* a,
   struct intersector_segment_pair const* b)
{
  ASSERT(a && b);
  return a->rank1 == b->rank1 && a->rank2 == b->rank2;
}
#define HTABLE_NAME interacting_segments
#define HTABLE_KEY struct intersector_segment_pair
#define HTABLE_DATA unsigned char
#define HTABLE_KEY_FUNCTOR_EQ pair_eq
#include <rsys/hash_table.h>

/* An htable to keep track of registered components */
#pragma pack(push, 1) /* Avoid padding */
struct intersector_component {
  scpr_polygon* polygon;
  uint32_t component;
  /* If there is padding, hashing reports uninitialized data in htable code */
};
#pragma pack(pop)
static FINLINE void
component_init(struct mem_allocator* alloc, struct intersector_component* key)
{
  ASSERT(alloc && key);
  (void)alloc;
  key->polygon = NULL;
  key->component = UINT32_MAX;
}
static INLINE char
component_eq
  (struct intersector_component const* a,
   struct intersector_component const* b)
{
  ASSERT(a && b);
  return a->polygon == b->polygon && a->component == b->component;
}
static FINLINE void
component_release(struct intersector_component* key)
{
  ASSERT(key);
  if(key->polygon) SCPR(polygon_ref_put(key->polygon));
}
static FINLINE res_T
component_copy
  (struct intersector_component* dst, struct intersector_component const* src)
{
  ASSERT(dst && src);
  dst->polygon = src->polygon;
  dst->component = src->component;
  if(dst->polygon) SCPR(polygon_ref_get(dst->polygon));
  return RES_OK;
}
static FINLINE res_T
component_copy_and_release
  (struct intersector_component* dst, struct intersector_component* src)
{
  ASSERT(dst && src);
  dst->polygon = src->polygon;
  dst->component = src->component;
  return RES_OK;
}
#define HTABLE_NAME registered_components
#define HTABLE_KEY struct intersector_component
#define HTABLE_DATA char
#define HTABLE_KEY_FUNCTOR_INIT component_init
#define HTABLE_KEY_FUNCTOR_EQ component_eq
#define HTABLE_KEY_FUNCTOR_COPY component_copy
#define HTABLE_KEY_FUNCTOR_RELEASE component_release
#define HTABLE_KEY_FUNCTOR_COPY_AND_RELEASE component_copy_and_release
#include <rsys/hash_table.h>

#define DARRAY_NAME components
#define DARRAY_DATA struct intersector_component
#define DARRAY_FUNCTOR_INIT component_init
#define DARRAY_FUNCTOR_EQ component_eq
#define DARRAY_FUNCTOR_COPY component_copy
#define DARRAY_FUNCTOR_RELEASE component_release
#define DARRAY_FUNCTOR_COPY_AND_RELEASE component_copy_and_release
#include <rsys/dynamic_array.h>

/* A set of segments from different polygons spatially sorted to help find
 * (self-)intersections. */
struct scpr_intersector {
  struct scpr_device* device;
  /* All the registered segments */
  struct darray_segments segments;
  /* Spatial partition of the segments along the 2 dimensions */
  struct intersector_dimension dimensions[2];
  /* All the pairs of segment that are in interaction in some way or another.
   * They are all the candidates for actual intersection plus some others that
   * are just close, but are without interaction. */
  struct htable_interacting_segments interacting_segments;
  /* All the registererd components (an htable to check for unicity and a darray
   * to store the components with indexing capability) */
  struct htable_registered_components registered_components;
  struct darray_components components;
  ref_T ref;
};

#endif
