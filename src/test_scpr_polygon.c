/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <memory.h>
#include <rsys/rsys.h>

int
main(int argc, char** argv)
{
  double coords0[] = {
    0.0, 0.0,
    0.5, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    1.0, 1.0,
    0.0, 1.0
  };
  double coords0_reduced[] = {
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0
  };
  double coords1[] = {
    0.0, 0.0,
    0.5, 0.0,
    0.0, 1.0,
    1.0, 1.0,
    1.0, 1000000000001, /* To be replaced */
    1.0, 0.0
  };
  double** coords;
  size_t nverts[] = { 6 };
  size_t ncomps = 1;
  double pos[2], range[2];
  size_t i, c, n;
  struct mem_allocator allocator;
  struct polygon_context ctx;
  struct scpr_polygon* polygon;
  struct scpr_polygon* copy;
  struct scpr_device* dev;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  int eq, in;
  double low[2] = {0, 0}, up[2] = {1, 1};
  FILE* stream = NULL;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  coords = (double**)MEM_CALLOC(&allocator, ncomps, sizeof(*coords));
  *coords = (double*)MEM_CALLOC(&allocator, nverts[0], 2*sizeof(**coords));
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));

  args.allocator = &allocator;
  args.precision = SCPR_DEVICE_CREATE_ARGS_DEFAULT.precision;
  OK(scpr_device_create(&args, &dev));

  /* Set out-of-range value in coords1 */
  SCPR(device_get_range(dev, range));
  coords1[9] = range[1] + 1;

  BAD(scpr_polygon_create(NULL, NULL));
  BAD(scpr_polygon_create(dev, NULL));
  OK(scpr_polygon_create(dev, &polygon));
  OK(scpr_polygon_create(dev, &copy));
  OK(scpr_polygon_ref_put(copy));

  BAD(scpr_polygon_ref_get(NULL));
  OK(scpr_polygon_ref_get(polygon));
  BAD(scpr_polygon_ref_put(NULL));
  OK(scpr_polygon_ref_put(polygon));
  OK(scpr_polygon_ref_put(polygon));

  OK(scpr_polygon_create(dev, &polygon));

  OK(scpr_polygon_get_components_count(polygon, &n));
  CHK(n == 0);

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.ncomps = ncomps;

  #define SETUP scpr_polygon_setup_indexed_vertices
  BAD(SETUP(NULL, ncomps, NULL, NULL, NULL));
  BAD(SETUP(polygon, ncomps, NULL, NULL, NULL));
  BAD(SETUP(NULL, ncomps, pget_nverts, NULL, NULL));
  BAD(SETUP(polygon, ncomps, pget_nverts, NULL, NULL));
  BAD(SETUP(NULL, ncomps, NULL, pget_pos, NULL));
  BAD(SETUP(polygon, ncomps, NULL, pget_pos, NULL));
  BAD(SETUP(NULL, ncomps, pget_nverts, pget_pos, NULL));
  BAD(SETUP(polygon, ncomps, pget_nverts, pget_pos, NULL));
  BAD(SETUP(NULL, ncomps, NULL, NULL, &ctx));
  BAD(SETUP(polygon, ncomps, NULL, NULL, &ctx));
  BAD(SETUP(NULL, ncomps, pget_nverts, NULL, &ctx));
  BAD(SETUP(polygon, ncomps, pget_nverts, NULL, &ctx));
  BAD(SETUP(NULL, ncomps, NULL, pget_pos, &ctx));
  BAD(SETUP(polygon, ncomps, NULL, pget_pos, &ctx));
  BAD(SETUP(NULL, ncomps, pget_nverts, pget_pos, &ctx));
  /* Check out of range data being detected at setup */
  memcpy(*coords, coords1, 2*nverts[0]*sizeof(**coords));
  BAD(SETUP(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  /* Check polygon was simplified to 4 vertices */
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  OK(SETUP(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_polygon_get_vertices_count(polygon, 0, &n));
  CHK(n == sizeof(coords0_reduced)/(2*sizeof(*coords0_reduced)));
  #undef SETUP

  BAD(scpr_polygon_in_bbox(NULL, NULL, NULL, NULL));
  BAD(scpr_polygon_in_bbox(NULL, NULL, NULL, &in));
  BAD(scpr_polygon_in_bbox(NULL, NULL, up, NULL));
  BAD(scpr_polygon_in_bbox(NULL, NULL, up, &in));
  BAD(scpr_polygon_in_bbox(NULL, low, NULL, NULL));
  BAD(scpr_polygon_in_bbox(NULL, low, NULL, &in));
  BAD(scpr_polygon_in_bbox(NULL, low, up, NULL));
  BAD(scpr_polygon_in_bbox(NULL, low, up, &in));
  BAD(scpr_polygon_in_bbox(polygon, NULL, NULL, NULL));
  BAD(scpr_polygon_in_bbox(polygon, NULL, NULL, &in));
  BAD(scpr_polygon_in_bbox(polygon, NULL, up, NULL));
  BAD(scpr_polygon_in_bbox(polygon, NULL, up, &in));
  BAD(scpr_polygon_in_bbox(polygon, low, NULL, NULL));
  BAD(scpr_polygon_in_bbox(polygon, low, NULL, &in));
  OK(scpr_polygon_in_bbox(polygon, low, up, &in));
  CHK(in);
  /* With smaller box */
  up[0] = 0.5;
  OK(scpr_polygon_in_bbox(polygon, low, up, &in));
  CHK(!in);

  BAD(scpr_polygon_get_components_count(NULL, NULL));
  BAD(scpr_polygon_get_components_count(polygon, NULL));
  BAD(scpr_polygon_get_components_count(NULL, &n));
  OK(scpr_polygon_get_components_count(polygon, &n));
  CHK(n == ncomps);

  BAD(scpr_polygon_get_vertices_count(NULL, ncomps, NULL));
  BAD(scpr_polygon_get_vertices_count(polygon, ncomps, NULL));
  BAD(scpr_polygon_get_vertices_count(NULL, 0, NULL));
  BAD(scpr_polygon_get_vertices_count(polygon, 0, NULL));
  BAD(scpr_polygon_get_vertices_count(NULL, ncomps, &n));
  BAD(scpr_polygon_get_vertices_count(polygon, ncomps, &n));
  BAD(scpr_polygon_get_vertices_count(NULL, 0, &n));
  OK(scpr_polygon_get_vertices_count(polygon, 0, &n));

  BAD(scpr_polygon_eq(NULL, NULL, NULL));
  BAD(scpr_polygon_eq(polygon, NULL, NULL));
  BAD(scpr_polygon_eq(NULL, polygon, NULL));
  BAD(scpr_polygon_eq(polygon, polygon, NULL));
  BAD(scpr_polygon_eq(NULL, NULL, &eq));
  BAD(scpr_polygon_eq(polygon, NULL, &eq));
  BAD(scpr_polygon_eq(polygon, 0, &eq));
  OK(scpr_polygon_eq(polygon, polygon, &eq));
  CHK(eq);

  BAD(scpr_polygon_get_position(NULL, ncomps, nverts[0], NULL));
  BAD(scpr_polygon_get_position(NULL, ncomps, nverts[0], pos));
  BAD(scpr_polygon_get_position(NULL, ncomps, 0, NULL));
  BAD(scpr_polygon_get_position(NULL, ncomps, 0, pos));
  BAD(scpr_polygon_get_position(NULL, 0, nverts[0], NULL));
  BAD(scpr_polygon_get_position(NULL, 0, nverts[0], pos));
  BAD(scpr_polygon_get_position(NULL, 0, 0, NULL));
  BAD(scpr_polygon_get_position(NULL, 0, 0, pos));
  BAD(scpr_polygon_get_position(polygon, ncomps, nverts[0], NULL));
  BAD(scpr_polygon_get_position(polygon, ncomps, nverts[0], pos));
  BAD(scpr_polygon_get_position(polygon, ncomps, 0, NULL));
  BAD(scpr_polygon_get_position(polygon, ncomps, 0, pos));
  BAD(scpr_polygon_get_position(polygon, 0, nverts[0], NULL));
  BAD(scpr_polygon_get_position(polygon, 0, nverts[0], pos));
  BAD(scpr_polygon_get_position(polygon, 0, 0, NULL));
  memcpy(*coords, coords0_reduced, sizeof(coords0_reduced));
  FOR_EACH(c, 0, ncomps) {
    size_t count;
    OK(scpr_polygon_get_vertices_count(polygon, c, &count));
    FOR_EACH(i, 0, count) {
      OK(scpr_polygon_get_position(polygon, c, i, pos));
      CHK(pos[0] == coords[c][i*2+0]);
      CHK(pos[1] == coords[c][i*2+1]);
    }
  }

  BAD(scpr_polygon_create_copy(NULL, NULL, NULL));
  BAD(scpr_polygon_create_copy(NULL, NULL, &copy));
  BAD(scpr_polygon_create_copy(NULL, polygon, NULL));
  OK(scpr_polygon_create_copy(dev, polygon, &copy));
  OK(scpr_polygon_eq(polygon, copy, &eq));
  CHK(eq);

  BAD(scpr_offset_polygon(NULL, 0, SCPR_JOIN_TYPES_COUNT__));
  BAD(scpr_offset_polygon(polygon, 0, SCPR_JOIN_TYPES_COUNT__));
  BAD(scpr_offset_polygon(NULL, 0, SCPR_JOIN_MITER));
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));

  BAD(scpr_polygon_dump_to_obj(NULL, NULL));
  BAD(scpr_polygon_dump_to_obj(polygon, NULL));
  BAD(scpr_polygon_dump_to_obj(NULL, stream));
  BAD(scpr_polygon_dump_to_obj(polygon, stream));
  stream = tmpfile();
  OK(scpr_polygon_dump_to_obj(polygon, stream));
  fclose(stream);
  stream = NULL;

  BAD(scpr_polygon_dump_component_to_obj(NULL, ncomps, NULL));
  BAD(scpr_polygon_dump_component_to_obj(polygon, ncomps, NULL));
  BAD(scpr_polygon_dump_component_to_obj(NULL, 0, NULL));
  BAD(scpr_polygon_dump_component_to_obj(NULL, ncomps, stream));
  BAD(scpr_polygon_dump_component_to_obj(polygon, ncomps, stream));
  BAD(scpr_polygon_dump_component_to_obj(polygon, 0, stream));
  stream = tmpfile();
  OK(scpr_polygon_dump_component_to_obj(polygon, 0, stream));
  fclose(stream);

  OK(scpr_device_ref_put(dev));
  OK(scpr_polygon_ref_put(polygon));
  OK(scpr_polygon_ref_put(copy));

  MEM_RM(&allocator, *coords);
  MEM_RM(&allocator, coords);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

