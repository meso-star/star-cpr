/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "scpr.h"
#include "scpr_c.h"

#include <polygon.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>
#include <rsys/double2.h>
#include <rsys/double3.h>

#include <stdlib.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
init_dimension
  (struct mem_allocator* allocator,
   struct intersector_dimension* dim,
   enum segment_interaction overlap,
   enum segment_interaction share)
{
  ASSERT(dim && allocator);
  htable_range_point_idx_init(allocator, &dim->unsorted);
  darray_range_points_init(allocator, &dim->sorted);
  dim->range[0] = INT64_MAX;
  dim->range[1] = INT64_MIN;
  dim->overlap= overlap;
  dim->contact = share;
}

static void
release_dimension
   (struct intersector_dimension* dim)
{
  ASSERT(dim);
  htable_range_point_idx_release(&dim->unsorted);
  darray_range_points_release(&dim->sorted);
}

static void
intersector_release(ref_T* ref)
{
  struct scpr_intersector* intersector;
  struct mem_allocator* allocator;
  ASSERT(ref);
  intersector = CONTAINER_OF(ref, struct scpr_intersector, ref);
  allocator = intersector->device->allocator;
  darray_segments_release(&intersector->segments);
  release_dimension(intersector->dimensions);
  release_dimension(intersector->dimensions + 1);
  SCPR(device_ref_put(intersector->device));
  htable_interacting_segments_release(&intersector->interacting_segments);
  htable_registered_components_release(&intersector->registered_components);
  darray_components_release(&intersector->components);
  MEM_RM(allocator, intersector);
}

/* Register a point (a segment end) against a dimension */
static res_T
register_point
  (struct scpr_intersector* intersector,
   const int dim_idx,
   const int64_t coord,
   const uint32_t seg_idx)
{
  uint32_t* pidx;
  uint32_t rp_idx;
  struct intersector_dimension* dim;
  struct intersector_range_point* sorted;
  res_T res = RES_OK;
  ASSERT(intersector);

  dim = intersector->dimensions + dim_idx;
  pidx = htable_range_point_idx_find(&dim->unsorted, &coord);
  if(!pidx) {
    /* First time this point: create it in sorted and store its index in unsorted */
    char one = 1;
    size_t count = darray_range_points_size_get(&dim->sorted);
    ERR(darray_range_points_resize(&dim->sorted, 1+count));
    sorted = darray_range_points_data_get(&dim->sorted);
    rp_idx = (uint32_t)count;
    sorted[rp_idx].coord = coord;
    /* Register this segment at this range point */
    ERR(htable_segment_idx_set(&sorted[rp_idx].segments, &seg_idx, &one));
    /* Update range */
    if(dim->range[0] > coord) dim->range[0] = coord;
    if(dim->range[1] < coord) dim->range[1] = coord;
  } else {
    /* This point is already known: look for seg_idx already there */
    char *found, one_or_two;
    rp_idx = *pidx;
    sorted = darray_range_points_data_get(&dim->sorted);
    found = htable_segment_idx_find(&sorted[rp_idx].segments, &seg_idx);
    one_or_two = found ? 2 : 1;
    ERR(htable_segment_idx_set(&sorted[rp_idx].segments, &seg_idx, &one_or_two));
  }
  ERR(htable_range_point_idx_set(&dim->unsorted, &coord, &rp_idx));

exit:
  return res;
error:
  goto exit;
}

/* Compare 2 range_points */
static int
compare_range_points(const void* a, const void* b)
{
  struct intersector_range_point *pa = (struct intersector_range_point*)a;
  struct intersector_range_point *pb = (struct intersector_range_point*)b;
  ASSERT(a && b && pa->coord != pb->coord);
  return (int)(pa->coord - pb->coord);
}

/* Sort the range points by increasing coordinates */
static void
sort_points
  (struct intersector_dimension* dim)
{
  size_t count;
  struct intersector_range_point* sorted;
  ASSERT(dim);

  /* All the range points are already in sorted; just need to sort them */
  count = darray_range_points_size_get(&dim->sorted);
  ASSERT(count == htable_range_point_idx_size_get(&dim->unsorted));
  sorted = darray_range_points_data_get(&dim->sorted);
  qsort(sorted, count, sizeof(*sorted), compare_range_points);
}

/* Create a segment pair suitable for use as an htable key
 * To ensure unicity, pairs are ordered */
static void
init_segment_pair
  (struct intersector_segment_pair* pair,
   const uint32_t seg_idx1,
   const uint32_t seg_idx2)
{
  ASSERT(pair);
  pair->rank1 = seg_idx1;
  pair->rank2 = seg_idx2;
  if(pair->rank1 > pair->rank2) {
    SWAP(uint32_t, pair->rank1, pair->rank2);
  }
}

#ifndef NDEBUG
static int
segments_bbox_interacts_1_core
  (int64_t lower1, int64_t upper1, int64_t lower2, int64_t upper2)
{
  if(lower1 > upper2) return 0;
  if(lower2 > upper1) return 0;
  return 1;
}

/* Check if 2 polygon Bbox interact */
static int
segments_bbox_interacts
  (Clipper2Lib::Point64 *v11,
   Clipper2Lib::Point64 *v12,
   Clipper2Lib::Point64 *v21,
   Clipper2Lib::Point64 *v22)
{
  int64_t lower1[2], upper1[2], lower2[2], upper2[2];
  ASSERT(v11 && v12 && v21 && v22);
  lower1[0] = MMIN(v11->x, v12->x);
  lower1[1] = MMIN(v11->y, v12->y);
  lower2[0] = MMIN(v21->x, v22->x);
  lower2[1] = MMIN(v21->y, v22->y);
  upper1[0] = MMAX(v11->x, v12->x);
  upper1[1] = MMAX(v11->y, v12->y);
  upper2[0] = MMAX(v21->x, v22->x);
  upper2[1] = MMAX(v21->y, v22->y);
  if(!segments_bbox_interacts_1_core(lower1[0], upper1[0], lower2[0], upper2[0]))
      return 0;
  return segments_bbox_interacts_1_core(lower1[1], upper1[1], lower2[1], upper2[1]);
}

static int
segments_bbox_interacts_1
  (uint32_t seg_idx1,
   uint32_t seg_idx2,
   enum segment_interaction inter,
   struct scpr_intersector* intersector)
{
  const struct intersector_segment *segments, *seg1, *seg2;
  Clipper2Lib::Point64 *v11, * v12, *v21, *v22;
  int64_t lower1, upper1, lower2, upper2;
  size_t count, r11, r12, r21, r22;
  const struct intersector_component* components;
  struct scpr_polygon *polygon1, *polygon2;
  uint32_t comp_idx1, comp_idx2;

  segments = darray_segments_cdata_get(&intersector->segments);
  seg1 = segments + seg_idx1;
  seg2 = segments + seg_idx2;
  components = darray_components_cdata_get(&intersector->components);
  comp_idx1 = components[seg1->component].component;
  comp_idx2 = components[seg2->component].component;
  polygon1 = components[seg1->component].polygon;
  polygon2 = components[seg2->component].polygon;

  /* Get vertices */
  r11 = seg1->first_vertex;
  SCPR(polygon_get_vertices_count(polygon1, comp_idx1, &count));
  r12 = (r11 + 1) % count;
  r21 = seg2->first_vertex;
  SCPR(polygon_get_vertices_count(polygon2, comp_idx2, &count));
  r22 = (r21 + 1) % count;
  v11 = &polygon1->paths[comp_idx1][r11];
  v12 = &polygon1->paths[comp_idx1][r12];
  v21 = &polygon2->paths[comp_idx2][r21];
  v22 = &polygon2->paths[comp_idx2][r22];
  if(inter & SOMETHING_X) {
    lower1 = MMIN(v11->x, v12->x);
    upper1 = MMAX(v11->x, v12->x);
    lower2 = MMIN(v21->x, v22->x);
    upper2 = MMAX(v21->x, v22->x);
  } else {
    lower1 = MMIN(v11->y, v12->y);
    upper1 = MMAX(v11->y, v12->y);
    lower2 = MMIN(v21->y, v22->y);
    upper2 = MMAX(v21->y, v22->y);
  }
  return segments_bbox_interacts_1_core(lower1, upper1, lower2, upper2);
}
#endif

/* Register interaction of 2 segments along 1 dimension */
static res_T
register_segment_interaction
  (const uint32_t seg_idx1,
   const uint32_t seg_idx2,
   const int allow_pair_creation,
   const enum segment_interaction inter,
   struct scpr_intersector* intersector)
{
  res_T res = RES_OK;
  struct intersector_segment_pair pair;
  unsigned char *pinteract;

  ASSERT(intersector);
  if(seg_idx1 != seg_idx2) {
    ASSERT(segments_bbox_interacts_1(seg_idx1, seg_idx2, inter, intersector));

    /* Fill a record for this pair of segments */
    init_segment_pair(&pair, seg_idx1, seg_idx2);
    pinteract =
      htable_interacting_segments_find(&intersector->interacting_segments, &pair);
    if(pinteract || allow_pair_creation) {
      unsigned char interact;
      if(!pinteract) { /* First occurence of this pair: create empty record */
        interact =  (unsigned char)inter;
      } else {
        ASSERT((*pinteract | inter) < UCHAR_MAX);
        interact = (unsigned char)(*pinteract | inter);
      }

      /* Register this interaction */
      ERR(htable_interacting_segments_set(&intersector->interacting_segments,
            &pair, &interact));
    }
  }

exit:
  return res;
error:
  goto exit;
}

/* Register interaction of segment of seg_idx with all the segments in
 * open_segments along 1 dimension */
static res_T
register_segment_interactions
  (const uint32_t seg_idx,
   struct htable_segment_idx* open_segments,
   const int allow_pair_creation,
   const enum segment_interaction inter,
   struct scpr_intersector* intersector)
{
  res_T res = RES_OK;
  struct htable_segment_idx_iterator it, end;

  ASSERT(open_segments && intersector);

  htable_segment_idx_begin(open_segments, &it);
  htable_segment_idx_end(open_segments, &end);
  /* Loop on segments using this point */
  while(!htable_segment_idx_iterator_eq(&it, &end)) {
    uint32_t seg2_idx = *htable_segment_idx_iterator_key_get(&it);
    ERR(register_segment_interaction(seg_idx, seg2_idx, allow_pair_creation,
          inter, intersector));
    htable_segment_idx_iterator_next(&it);
  }

exit:
  return res;
error:
  goto exit;
}

/* Register segments interaction along 1 dimension */
static res_T
register_interaction
  (const int dim_idx,
   struct scpr_intersector* intersector)
{
  res_T res = RES_OK;
  size_t count, i;
  struct intersector_range_point* points;
  struct htable_segment_idx open_segments;
  struct mem_allocator* allocator;
  struct intersector_dimension* dimension;
  char *begins = NULL, *ends = NULL;
  uint32_t* seg_index = NULL;
  ASSERT(intersector);

  allocator = intersector->device->allocator;
  dimension = intersector->dimensions + dim_idx;
  htable_segment_idx_init(allocator, &open_segments);
  count = darray_range_points_size_get(&dimension->sorted);
  points = darray_range_points_data_get(&dimension->sorted);
  for(i = 0; i < count; i++) {
    struct intersector_range_point* point = points + i;
    struct htable_segment_idx_iterator it, end;
    size_t scount, s;

    /* 0) Init begins, ends and seg_index
     * Cannot recompute them on the fly as found will change */
    scount = htable_segment_idx_size_get(&point->segments);
    begins = (char*)MEM_REALLOC(allocator, begins, scount * sizeof(*begins));
    ends = (char*)MEM_REALLOC(allocator, ends, scount * sizeof(*ends));
    seg_index = (uint32_t*)MEM_REALLOC(allocator, seg_index,
        scount * sizeof(*seg_index));
    if(!begins || !ends || !seg_index) {
      res = RES_MEM_ERR;
      goto error;
    }
    s = 0;
    htable_segment_idx_begin(&point->segments, &it);
    htable_segment_idx_end(&point->segments, &end);
    while(!htable_segment_idx_iterator_eq(&it, &end)) {
      char c = *htable_segment_idx_iterator_data_get(&it);
      uint32_t seg_idx = *htable_segment_idx_iterator_key_get(&it);
      if(c == 2) { /* Both ends of the segment at this point */
        ends[s] = begins[s] = 1;
      } else {
        char* found = htable_segment_idx_find(&open_segments, &seg_idx);
        begins[s] = (found == NULL);
        ends[s] = (found != NULL);
      }
      seg_index[s] = seg_idx;
      s++;
      htable_segment_idx_iterator_next(&it);
    }
    ASSERT(s == scount);

    /* 1) Segments beginning and ending at this point overlap other segments that
     * begin and end at the same point and contact segments that only begin or
     * only end at this point */
    for(s = 0; s < scount; s++) {
      if(begins[s] && ends[s]) {
        size_t j;
        for(j = 0; j < scount; j++) {
          if(j > s && begins[j] && ends[j]) {
            ERR(register_segment_interaction(seg_index[s], seg_index[j], !dim_idx,
                  dimension->overlap, intersector));
          }
          else if(begins[j] ^ ends[j]) {
            ERR(register_segment_interaction(seg_index[s], seg_index[j], !dim_idx,
                  dimension->contact, intersector));
          }
        }
      }
    }

    /* 2) Segments only ending at this point overlap open segments */
    for(s = 0; s < scount; s++) {
      if(!begins[s] && ends[s]) {
        ERR(register_segment_interactions(seg_index[s], &open_segments, !dim_idx,
              dimension->overlap, intersector));
      }
    }

    /* 3) Segments ending at this point are now closed */
    for(s = 0; s < scount; s++) {
      if(ends[s]) {
        size_t n = htable_segment_idx_erase(&open_segments, seg_index + s);
        CHK(n == (begins[s] ? 0 : 1));
      }
    }

    /* 4) Segments beginning and ending at this point overlap open segments */
    for(s = 0; s < scount; s++) {
      if(begins[s] && ends[s]) {
        ERR(register_segment_interactions(seg_index[s], &open_segments, !dim_idx,
              dimension->overlap, intersector));
      }
    }

    /* 5) Segments only beginning at this point are now open */
    for(s = 0; s < scount; s++) {
      if(begins[s] && !ends[s]) {
        char one = 1;
        ERR(htable_segment_idx_set(&open_segments, seg_index + s, &one));
      }
    }

    /* 6) Segments only beginning at this point overlap open segments */
    for(s = 0; s < scount; s++) {
      if(begins[s] && !ends[s]) {
        ERR(register_segment_interactions(seg_index[s], &open_segments, !dim_idx,
              dimension->overlap, intersector));
      }
    }
  }

exit:
  MEM_RM(allocator, begins);
  MEM_RM(allocator, ends);
  MEM_RM(allocator, seg_index);
  htable_segment_idx_release(&open_segments);
  return res;
error:
  goto exit;
}

static int64_t
safe_mul
  (int64_t a,
   int64_t b,
   int* ovflw)
{
  int64_t r;
  if(b == 0) return 0;
  r = a * b;
  if (r / b != a) *ovflw = 1;
  return r;
}

/* Check if a triangle is CCW (>0), CW (<0), or flat (=0) */
static int
is_ccw
  (Clipper2Lib::Point64* v1,
   Clipper2Lib::Point64* v2,
   Clipper2Lib::Point64* v3,
   int* ovflw)
{
  int64_t tmp1, tmp2;
  ASSERT(v1 && v2 && v3);
  /* Limited coordinate range garanties that sub computations stay in int64_t
   * range */
   tmp1 = safe_mul(v2->x - v1->x, v3->y - v1->y, ovflw);
   tmp2 = safe_mul(v3->x - v1->x, v2->y - v1->y, ovflw);
   if(tmp1 < tmp2) return -1;
   if(tmp1 > tmp2) return +1;
   return 0;
}

/* Check if 2 segments intersect */
static res_T
check_two_segments_intersection
  (const struct intersector_segment* seg1,
   const struct intersector_segment* seg2,
   struct scpr_intersector_check_callbacks* callbacks,
   struct scpr_intersector* intersector,
   void* data)
{
  res_T res = RES_OK;
  size_t count, r11, r12, r21, r22;
  Clipper2Lib::Point64 *v11, * v12, *v21, *v22;
  int ccw1, ccw2, ccw3, ccw4, ovflw = 0;
  const struct intersector_component* components;
  struct scpr_polygon *polygon1, *polygon2;
  uint32_t comp_idx1, comp_idx2;

  ASSERT(seg1 && seg2 && callbacks);

  components = darray_components_cdata_get(&intersector->components);
  comp_idx1 = components[seg1->component].component;
  comp_idx2 = components[seg2->component].component;
  polygon1 = components[seg1->component].polygon;
  polygon2 = components[seg2->component].polygon;

  /* Get vertices */
  r11 = seg1->first_vertex;
  ERR(scpr_polygon_get_vertices_count(polygon1, comp_idx1, &count));
  r12 = (r11 + 1) % count;
  r21 = seg2->first_vertex;
  ERR(scpr_polygon_get_vertices_count(polygon2, comp_idx2, &count));
  r22 = (r21 + 1) % count;
  v11 = &polygon1->paths[comp_idx1][r11];
  v12 = &polygon1->paths[comp_idx1][r12];
  v21 = &polygon2->paths[comp_idx2][r21];
  v22 = &polygon2->paths[comp_idx2][r22];
  ASSERT(segments_bbox_interacts(v11, v12, v21, v22));

  /* Test triangles orientation */
  ccw1 = is_ccw(v11, v12, v21, &ovflw);
  ccw2 = is_ccw(v11, v12, v22, &ovflw);
  ccw3 = is_ccw(v21, v22, v11, &ovflw);
  ccw4 = is_ccw(v21, v22, v12, &ovflw);
  if(ovflw) {
    res = RES_BAD_ARG;
    goto error;
  }
  /* Analyse ccw results */
  if(ccw1 * ccw2 > 0 && ccw3 * ccw4 > 0) {
    /* No intersection at all */
  }
  else if(ccw1 == 0 && ccw2 == 0 && ccw3 == 0 && ccw4 == 0) {
    /* overlapping segments */
    if(callbacks->overlapping_segments) {
      struct scpr_callback_segment arg1, arg2;
      arg1.polygon = polygon1;
      arg1.component = comp_idx1;
      arg1.first_vertex = r11;
      arg1.last_vertex = r12;
      arg2.polygon = polygon2;
      arg2.component = comp_idx2;
      arg2.first_vertex = r21;
      arg2.last_vertex = r22;
      if(callbacks->overlapping_segments(&arg1, &arg2, data)) {
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }
  else if(ccw1 * ccw2 < 0 && ccw3 * ccw4 < 0) {
    /* Basic segment intersection */
    if(callbacks->simple_intersection) {
      struct scpr_callback_segment arg1, arg2;
      arg1.polygon = polygon1;
      arg1.component = comp_idx1;
      arg1.first_vertex = r11;
      arg1.last_vertex = r12;
      arg2.polygon = polygon2;
      arg2.component = comp_idx2;
      arg2.first_vertex = r21;
      arg2.last_vertex = r22;
      if(callbacks->simple_intersection(&arg1, &arg2, data)) {
        res = RES_BAD_ARG;
        goto error;
      }
    }
}

exit:
  return res;
error:
  goto exit;
}

/* Test intersection of all the registered pairs of interacting segments */
static res_T
check_interacting_pairs
  (struct scpr_intersector* intersector,
   struct scpr_intersector_check_callbacks* callbacks,
   void* data)
{
  res_T res = RES_OK;
  struct htable_interacting_segments_iterator it, end;
  const struct intersector_segment* segments;

  ASSERT(intersector && callbacks);

  segments = darray_segments_cdata_get(&intersector->segments);
  htable_interacting_segments_begin(&intersector->interacting_segments, &it);
  htable_interacting_segments_end(&intersector->interacting_segments, &end);
  /* Loop on interacting segments */
  while(!htable_interacting_segments_iterator_eq(&it, &end)) {
    unsigned char overlapping
      = *htable_interacting_segments_iterator_data_get(&it);
    if(overlapping & SOME_OVERLAP
        && overlapping & SOMETHING_X && overlapping & SOMETHING_Y)
    {
      struct intersector_segment_pair* pair
        = htable_interacting_segments_iterator_key_get(&it);
      const struct intersector_segment* seg1 = segments + pair->rank1;
      const struct intersector_segment* seg2 = segments + pair->rank2;
      ERR(check_two_segments_intersection(seg1, seg2, callbacks, intersector, data));
    }
    htable_interacting_segments_iterator_next(&it);
  }

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
scpr_intersector_create
  (struct scpr_device* device,
   struct scpr_intersector** out_intersector)
{
  res_T res = RES_OK;
  struct scpr_intersector* intersector = NULL;
  struct mem_allocator* allocator;

  if(!device || ! out_intersector) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = device->allocator;
  intersector = (struct scpr_intersector*)MEM_ALLOC(allocator, sizeof(*intersector));
  if(!intersector) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&intersector->ref);
  intersector->device = device;
  ERR(scpr_device_ref_get(device));
  darray_segments_init(allocator, &intersector->segments);
  init_dimension(allocator, intersector->dimensions, OVERLAP_X, CONTACT_X);
  init_dimension(allocator, intersector->dimensions + 1, OVERLAP_Y, CONTACT_Y);
  htable_interacting_segments_init(allocator, &intersector->interacting_segments);
  htable_registered_components_init(allocator, &intersector->registered_components);
  darray_components_init(allocator, &intersector->components);

exit:
  if(out_intersector) *out_intersector = intersector;
  return res;
error:
  if(intersector) {
    SCPR(intersector_ref_put(intersector));
    intersector = NULL;
  }
  goto exit;
}

res_T
scpr_intersector_ref_get
  (struct scpr_intersector* intersector)
{
  if(!intersector) return RES_BAD_ARG;
  ref_get(&intersector->ref);
  return RES_OK;
}

res_T
scpr_intersector_ref_put
  (struct scpr_intersector* intersector)
{
  if(!intersector) return RES_BAD_ARG;
  ref_put(&intersector->ref, intersector_release);
  return RES_OK;
}

res_T
scpr_intersector_register_polygon
  (struct scpr_intersector* intersector,
   struct scpr_polygon* polygon)
{
  res_T res = RES_OK;
  size_t i;

  if(!intersector || !polygon) {
    res = RES_BAD_ARG;
    goto error;
  }

  for(i = 0; i < polygon->paths.size(); i++) {
    ERR(scpr_intersector_register_component(intersector, polygon, i));
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_intersector_register_component
  (struct scpr_intersector* intersector,
   struct scpr_polygon* polygon,
   const size_t icomponent)
{
  res_T res = RES_OK;
  uint32_t i;
  size_t new_count, prev_count, c;
  struct intersector_component comp;
  char *found, one = 1;

  if(!intersector || !polygon || icomponent >= polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Check single registration */
  comp.polygon = polygon;
  comp.component = (uint32_t)icomponent;
  found = htable_registered_components_find(&intersector->registered_components,
        &comp);
  if(found) {
    logger_print(intersector->device->logger, LOG_ERROR,
        "Component %zu already registered.\n", icomponent);
    res = RES_BAD_ARG;
    goto error;
  }
  c = htable_registered_components_size_get(&intersector->registered_components);
  if(c >= UINT32_MAX) {
    logger_print(intersector->device->logger, LOG_ERROR,
        "Too many components registered.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  ASSERT(c == darray_components_size_get(&intersector->components));
  ERR(htable_registered_components_set(&intersector->registered_components,
        &comp, &one));
  ERR(darray_components_push_back(&intersector->components, &comp));

  prev_count = darray_segments_size_get(&intersector->segments);
  ASSERT(prev_count <= UINT32_MAX);
  new_count = polygon->paths[icomponent].size();
  if(prev_count + new_count > UINT32_MAX) {
    logger_print(intersector->device->logger, LOG_ERROR,
        "Too many segments registered.\n");
    res = RES_BAD_ARG;
    goto error;
  }
  ERR(darray_segments_resize(&intersector->segments,
        (uint32_t)(prev_count + new_count)));

  for(i = 0; i < new_count; i++) {
    Clipper2Lib::Point64 *p0, *p1;
    uint32_t seg_idx = (uint32_t)(prev_count + i);
    struct intersector_segment* segment
      = darray_segments_data_get(&intersector->segments) + seg_idx;
    segment->component = (uint32_t)c;
    segment->first_vertex = i;
    p0 = &polygon->paths[icomponent][i];
    p1 = &polygon->paths[icomponent][(i+1)%new_count];
    ERR(register_point(intersector, 0, p0->x, seg_idx));
    ERR(register_point(intersector, 0, p1->x, seg_idx));
    ERR(register_point(intersector, 1, p0->y, seg_idx));
    ERR(register_point(intersector, 1, p1->y, seg_idx));
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_intersector_check
  (struct scpr_intersector* intersector,
   struct scpr_intersector_check_callbacks* callbacks,
   void* data)
{
  int i;
  res_T res = RES_OK;

  if(!intersector || !callbacks
      || (!callbacks->simple_intersection && !callbacks->overlapping_segments))
  {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Construct candidates for intersection */
  for(i = 0; i < 2; i++) {
    sort_points(&intersector->dimensions[i]);
    ERR(register_interaction(i, intersector));
  }
  /* Check intersections */
  ERR(check_interacting_pairs(intersector, callbacks, data));

exit:
  return res;
error:
  goto exit;
}
