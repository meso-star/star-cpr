/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_CPR_UTILS_H
#define TEST_CPR_UTILS_H

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include <rsys/mem_allocator.h>
#include <stdio.h>
#include <math.h>

#define ERR(Expr) { if((res = (Expr)) != RES_OK) goto error; } (void)0
#define BAD(Expr) CHK((Expr) == RES_BAD_ARG);
#define OK(Expr) CHK((Expr) == RES_OK);

struct polygon_context {
  double** coords;
  size_t* nverts;
  size_t ncomps;
};

static INLINE void
pget_nverts(const size_t icomp, size_t* nverts, void* context)
{
  const struct polygon_context* ctx = (const struct polygon_context*)context;
  CHK(nverts != NULL);
  CHK(context != NULL);
  CHK(icomp < ctx->ncomps);
  *nverts = ctx->nverts[icomp];
}

static INLINE void
pget_pos(const size_t icomp, const size_t ivert, double pos[2], void* context)
{
  const struct polygon_context* ctx = (const struct polygon_context*)context;
  CHK(pos != NULL);
  CHK(context != NULL);
  CHK(ctx->coords != NULL);
  CHK(icomp < ctx->ncomps);
  CHK(ivert < ctx->nverts[icomp]);
  pos[0] = ctx->coords[icomp][ivert*2 + 0];
  pos[1] = ctx->coords[icomp][ivert*2 + 1];
}

struct mesh_context {
  const double* coords;
  size_t nverts;
  const size_t* indices;
  size_t ntris;
};

static INLINE void
mget_pos(const size_t ivert, double pos[2], void* context)
{
  const struct mesh_context* ctx = (const struct mesh_context*)context;
  CHK(pos != NULL);
  CHK(context != NULL);
  CHK(ctx->coords != NULL);
  CHK(ivert < ctx->nverts);
  pos[0] = ctx->coords[ivert*2 + 0];
  pos[1] = ctx->coords[ivert*2 + 1];
}

static INLINE void
mget_ids(const size_t itri, size_t ids[3], void* context)
{
  const struct mesh_context* ctx = (const struct mesh_context*)context;
  CHK(ids != NULL);
  CHK(context != NULL);
  CHK(ctx->indices != NULL);
  CHK(itri < ctx->ntris);
  ids[0] = ctx->indices[itri*3 + 0];
  ids[1] = ctx->indices[itri*3 + 1];
  ids[2] = ctx->indices[itri*3 + 2];
}

static void
check_memory_allocator(struct mem_allocator* allocator)
{
  if(MEM_ALLOCATED_SIZE(allocator)) {
    char dump[512];
    MEM_DUMP(allocator, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks\n");
  }
}

INLINE int
check_stability
  (struct scpr_device* dev,
   const struct scpr_polygon* polygon)
{
  res_T res = RES_OK;
  size_t i, j, ccount, vcount;
  ASSERT(dev && polygon);

  ERR(scpr_polygon_get_components_count(polygon, &ccount));

  for(i = 0; i < ccount; i++) {
    ERR(scpr_polygon_get_vertices_count(polygon, i, &vcount));
    for(j = 0; j < vcount; j++) {
      double pt[2], tmp[2];
      int64_t tmp2[2];
      ERR(scpr_polygon_get_position(polygon, i, j, pt));
      ERR(scpr_device_scale(dev, pt, 2, tmp2));
      ERR(scpr_device_unscale(dev, tmp2, 2, tmp));
      if(tmp[0] != pt[0] || tmp[1] != pt[1]) {
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }
end:
  return (res == RES_OK);
error:
  goto end;
}

#endif /* TEST_CPR_UTILS_H */
