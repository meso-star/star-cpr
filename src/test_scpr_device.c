/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <rsys/rsys.h>

#include <memory.h>

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  double r[2];
  const double d[5] = { 0, 1, 2, 3, 4};
  double tmpd[5];
  const int64_t i64[5] = { 0, 1, 2, 3, 4};
  int64_t tmp64[5];
  int in;
  (void)argc, (void)argv;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  args.allocator = &allocator;

  BAD(scpr_device_create(NULL, NULL));
  BAD(scpr_device_create(&args, NULL));
  BAD(scpr_device_create(NULL, &dev));

  args.precision = -1;
  BAD(scpr_device_create(&args, &dev));

  args.precision = SCPR_DEVICE_CREATE_ARGS_DEFAULT.precision;
  OK(scpr_device_create(&args, &dev));

  BAD(scpr_device_ref_get(NULL));
  OK(scpr_device_ref_get(dev));

  BAD(scpr_device_ref_put(NULL));
  OK(scpr_device_ref_put(dev));

  BAD(scpr_device_get_range(NULL, NULL));
  BAD(scpr_device_get_range(NULL, r));
  BAD(scpr_device_get_range(dev, NULL));
  OK(scpr_device_get_range(dev, r));

  BAD(scpr_device_in_range(NULL, NULL, 5, NULL));
  BAD(scpr_device_in_range(NULL, NULL, 5, &in));
  BAD(scpr_device_in_range(NULL, d, 5, NULL));
  BAD(scpr_device_in_range(dev, NULL, 5, NULL));
  BAD(scpr_device_in_range(NULL, d, 5, &in));
  BAD(scpr_device_in_range(dev, NULL, 5, &in));
  BAD(scpr_device_in_range(dev, d, 5, NULL));
  OK(scpr_device_in_range(dev, d, 5, &in));
  CHK(in);
  /* With out_of_range value */
  memcpy(tmpd, d, sizeof(d));
  tmpd[3] = r[1] + 1;
  OK(scpr_device_in_range(dev, tmpd, 5, &in));
  CHK(!in);

  BAD(scpr_device_scale(NULL, NULL, 5, NULL));
  BAD(scpr_device_scale(NULL, NULL, 5, tmp64));
  BAD(scpr_device_scale(NULL, d, 5, NULL));
  BAD(scpr_device_scale(dev, NULL, 5, NULL));
  BAD(scpr_device_scale(NULL, d, 5, tmp64));
  BAD(scpr_device_scale(dev, NULL, 5, tmp64));
  BAD(scpr_device_scale(dev, d, 5, NULL));
  OK(scpr_device_scale(dev, d, 5, tmp64));
  /* With out_of_range value */
  memcpy(tmpd, d, sizeof(d));
  tmpd[3] = r[1] + 1;
  BAD(scpr_device_scale(dev, tmpd, 5, tmp64));

  BAD(scpr_device_unscale(NULL, NULL, 5, NULL));
  BAD(scpr_device_unscale(NULL, NULL, 5, tmpd));
  BAD(scpr_device_unscale(NULL, i64, 5, NULL));
  BAD(scpr_device_unscale(dev, NULL, 5, NULL));
  BAD(scpr_device_unscale(NULL, i64, 5, tmpd));
  BAD(scpr_device_unscale(dev, NULL, 5, tmpd));
  BAD(scpr_device_unscale(dev, i64, 5, NULL));
  OK(scpr_device_unscale(dev, i64, 5, tmpd));

  OK(scpr_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

