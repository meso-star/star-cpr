/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCPR_H
#define SCPR_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SCPR_SHARED_BUILD) /* Build shared library */
  #define SCPR_API extern EXPORT_SYM
#elif defined(SCPR_STATIC_BUILD) /* Use/build static library */
  #define SCPR_API extern LOCAL_SYM
#else /* Use shared library */
  #define SCPR_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the scpr function `Func'
 * returns an error. */
#ifndef NDEBUG
  #define SCPR(Func) ASSERT(scpr_##Func == RES_OK)
#else
  #define SCPR(Func) scpr_##Func
#endif

enum scpr_operation {
  SCPR_AND, /* Keep the mesh part that intersects the clip polygon */
  SCPR_SUB, /* Remove the mesh part that intersects the clip polygon */
  SCPR_OPERATIONS_COUNT__
};

enum scpr_join_type {
  SCPR_JOIN_SQUARE,
  SCPR_JOIN_ROUND,
  SCPR_JOIN_MITER,
  SCPR_JOIN_TYPES_COUNT__
};

/* Forward declaration */
struct mem_allocator;

/* Forward declaration of the star-cpr opaque data types. These data types are
 * ref counted. Once created the caller implicitly owns the created data, i.e.
 * its reference counter is set to 1. The scpr_<TYPE>_ref_<get|put> functions
 * get or release a reference on the data, i.e. they increment or decrement the
 * reference counter, respectively. When this counter reaches 0, the object is
 * silently destroyed and cannot be used anymore. */
struct scpr_device;
struct scpr_polygon;
struct scpr_mesh;
struct scpr_intersector;

/* Input arguments of the scpr_device_create function */
struct scpr_device_create_args {
  struct logger* logger; /* NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> default allocator */
  int verbosity_level;
  int precision; /* Number of decimal place preserved; in [0 8] */
};
#define SCPR_DEVICE_CREATE_ARGS_DEFAULT__ { \
  NULL, NULL, 1, 6 \
}
static const struct scpr_device_create_args SCPR_DEVICE_CREATE_ARGS_DEFAULT =
  SCPR_DEVICE_CREATE_ARGS_DEFAULT__;

/* A struct to represent segments when calling user callbacks. */
struct scpr_callback_segment {
  struct scpr_polygon* polygon;
  size_t component;
  size_t first_vertex;
  size_t last_vertex;
};

/* A struct holding callbacks that are called when carying an intersection check
 * on a bunch of segments.
 * If a callbacks is NULL, the corresponding events are unreported, but at least
 * 1 callback must be non NULL.
 * If a callback call returns non-zero, the whole scpr_intersector_check call
 * exits whith a RES_BAD_ARG error status without further processing. */
struct scpr_intersector_check_callbacks {
  int(*simple_intersection)
    (struct scpr_callback_segment* segment1,
     struct scpr_callback_segment* segment2,
     void* ctx);
  int(*overlapping_segments)
    (struct scpr_callback_segment* segment1,
     struct scpr_callback_segment* segment2,
     void* ctx);
};
#define SCPR_INTERSECTOR_CHECK_CALLBACKS_NULL__ { NULL, NULL }

BEGIN_DECLS

/*******************************************************************************
 * star-scpr Device. It is an handle toward the Stardis library. It manages the
 * star-scpr resources.
 ******************************************************************************/
SCPR_API res_T
scpr_device_create
  (const struct scpr_device_create_args* args,
   struct scpr_device** dev);

SCPR_API res_T
scpr_device_ref_get
  (struct scpr_device* dev);

SCPR_API res_T
scpr_device_ref_put
  (struct scpr_device* dev);

/* Get the range for polygon and mesh vertices.
 * The range depends on the precision parameter of the device and is
 * [-2^(46-precision) + 2^(46-precision)]. */
SCPR_API res_T
scpr_device_get_range
  (const struct scpr_device* dev,
   double range[2]);

/* Check if values are in range */
SCPR_API res_T
scpr_device_in_range
  (const struct scpr_device* dev,
   const double* values,
   const size_t count,
   int* in_range);

/* Scale a set of values.
 * Internal representation for vertices in star-cpr is in interger format.
 * The process of converting reals to integers is named scaling (and the reverse
 * process is named unscaling). */
SCPR_API res_T
scpr_device_scale
  (const struct scpr_device* dev,
   const double* values,
   const size_t count,
   int64_t* scaled);

/* Unscale a set of values.
 * Internal representation for vertices in star-cpr is in interger format.
 * The process of converting reals to integers is named scaling (and the reverse
 * process is named unscaling). */
SCPR_API res_T
scpr_device_unscale
  (const struct scpr_device* dev,
   const int64_t* values,
   const size_t count,
   double* unscaled);

/*******************************************************************************
 * Type of polygons, as manipulated by star-cpr.
 * Polygons can be made of any number of paths and are subject to a range limit
 * and a precision (see device).
 * Polygons inside/outside regions are defined by their winding numbers
 * considering the Even-Odd convention.
 * E.g. a CCW path inside a CW one defines a hole.
 ******************************************************************************/
SCPR_API res_T
scpr_polygon_create
  (struct scpr_device* dev,
   struct scpr_polygon** polygon);

SCPR_API res_T
scpr_polygon_create_copy
  (struct scpr_device* dev,
   const struct scpr_polygon* src_polygon,
   struct scpr_polygon** polygon);

SCPR_API res_T
scpr_polygon_ref_get
  (struct scpr_polygon* polygon);

SCPR_API res_T
scpr_polygon_ref_put
  (struct scpr_polygon* polygon);

/* The number of components and vertices can be changed due to a
 * simplification process and one should not take for granted that the number of
 * components and vertices stay as provided at construction time.
 * The actual counts and coordinates should be retrieved using the appropriate
 * getters. */
SCPR_API res_T
scpr_polygon_setup_indexed_vertices
  (struct scpr_polygon* polygon,
   const size_t ncomponents, /* #connex components */
   void (*get_nverts)(const size_t icomponent, size_t *nverts, void* ctx),
   void (*get_position)
     (const size_t icomponent, const size_t ivert, double pos[2], void* ctx),
   void* data); /* Client data set as the last param of the callbacks */

SCPR_API res_T
scpr_polygon_in_bbox
  (struct scpr_polygon* polygon,
   const double lower[2],
   const double upper[2],
   int* inside);

SCPR_API res_T
scpr_offset_polygon
  (struct scpr_polygon* polygon,
   const double offset, /* Can be either positive or negative */
   const enum scpr_join_type join_type);

SCPR_API res_T
scpr_polygon_get_components_count
  (const struct scpr_polygon* polygon,
   size_t* ncomps);

SCPR_API res_T
scpr_polygon_get_vertices_count
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   size_t* nverts);

SCPR_API res_T
scpr_polygon_get_position
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   const size_t ivert,
   double position[2]);

/* Get the polygon component orientation.
 * Only meaningful for simple polygons. */
SCPR_API res_T
scpr_polygon_is_component_cw
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   int* cw);

/* Reverse the polygon component orientation.
 * Only meaningful for simple polygons. */
SCPR_API res_T
scpr_polygon_reverse_component
  (struct scpr_polygon* polygon,
   const size_t icomponent);

/* Logical comparison for polygons.
 * Component order and orientation are not considered. */
SCPR_API res_T
scpr_polygon_eq
  (const struct scpr_polygon* polygon1,
   const struct scpr_polygon* polygon2,
   int* is_eq);

/* Return a vertex that is inside a component. */
SCPR_API res_T
scpr_get_vertex_in_component
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   double vertex[2]);

/* Check if a vertex is in a component. */
SCPR_API res_T
scpr_is_vertex_in_component
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   const double vertex[2],
   int* situation); /* +1: inside, 0: on, -1: outside */

/* Check if a component is inside a component, given the 2 components are not
 * equal and do not overlap (they can be adjoining).
 * Only meaningful for simple components. */
SCPR_API res_T
scpr_is_component_in_component
  (const struct scpr_polygon* polygon1,
   const size_t icomponent1,
   const struct scpr_polygon* polygon2,
   const size_t icomponent2,
   int* c1_is_in_c2);

SCPR_API res_T
scpr_polygon_dump_to_obj
  (struct scpr_polygon* polygon,
   FILE* stream);

SCPR_API res_T
scpr_polygon_dump_component_to_obj
  (struct scpr_polygon* polygon,
   const size_t icomponent,
   FILE* stream);

/*******************************************************************************
 * Type of meshes, as manipulated by star-cpr.
 * Meshes can be made of any number of triangles and are subject to a range
 * limit and a precision (see device).
 ******************************************************************************/
SCPR_API res_T
scpr_mesh_create
  (struct scpr_device* dev,
   struct scpr_mesh** mesh);

SCPR_API res_T
scpr_mesh_ref_get
  (struct scpr_mesh* mesh);

SCPR_API res_T
scpr_mesh_ref_put
  (struct scpr_mesh* mesh);

SCPR_API res_T
scpr_mesh_setup_indexed_vertices
  (struct scpr_mesh* mesh,
   const size_t ntris, /* #triangles */
   void (*get_indices)(const size_t itri, size_t ids[3], void* ctx),
   const size_t nverts, /* #vertices */
   void (*get_position)(const size_t ivert, double pos[2], void* ctx),
   void* data); /* Client data set as the last param of the callbacks */

/* Clip the mesh against the polygon using the EvenOdd filling rule. */
SCPR_API res_T
scpr_mesh_clip
  (struct scpr_mesh* mesh, /* Candidate mesh to clip */
   const enum scpr_operation op, /* Clip operation */
   struct scpr_polygon* polygon); /* Clip polygon */

SCPR_API res_T
scpr_mesh_get_triangles_count
  (const struct scpr_mesh* mesh,
   size_t* ntris);

SCPR_API res_T
scpr_mesh_get_vertices_count
  (const struct scpr_mesh* mesh,
   size_t* nverts);

SCPR_API res_T
scpr_mesh_get_indices
  (const struct scpr_mesh* mesh,
   const size_t itri,
   size_t ids[3]);

SCPR_API res_T
scpr_mesh_get_position
  (const struct scpr_mesh* mesh,
   const size_t ivert,
   double position[2]);

/*******************************************************************************
 * An intersector provide a way to check for polygon intersections, either
 * self intersections or intersections between polygons.
 ******************************************************************************/
SCPR_API res_T
scpr_intersector_create
  (struct scpr_device* dev,
   struct scpr_intersector** intersector);

SCPR_API res_T
scpr_intersector_ref_get
  (struct scpr_intersector* intersector);

SCPR_API res_T
scpr_intersector_ref_put
  (struct scpr_intersector* intersector);

/* Register a polygon for further analysis */
SCPR_API res_T
scpr_intersector_register_polygon
  (struct scpr_intersector* intersector,
   struct scpr_polygon* polygon);

/* Register a polygon's component for further analysis */
SCPR_API res_T
scpr_intersector_register_component
  (struct scpr_intersector* intersector,
   struct scpr_polygon* polygon,
   const size_t icomponent);

SCPR_API res_T
scpr_intersector_check
  (struct scpr_intersector* intersector,
   struct scpr_intersector_check_callbacks* callbacks,
   void* data); /* Client data set as the last param of the callbacks */

END_DECLS

#endif /* SCPR_H */

