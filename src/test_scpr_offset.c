/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "scpr.h"
#include "test_scpr_utils.h"

#include <memory.h>

static void
test_single(void)
{
  const double coords0[] = {
    0.0, 0.0,
    0.0, 1.0,
    1.0, 1.0,
    1.0, 0.0
  };
  const double coords1[] = {
    -1.0, -1.0,
    -1.0, 2.0,
    2.0, 2.0,
    2.0, -1.0
  };
  double coords2[] = {
    0.12345678901234, 0.0,
    0.0, 1.0,
    1.0, 1000000000000, /* To be replaced */
    1.0, 0.0
  };
  double coords2_reverse[] = {
    0.12345678901234, 0.0,
    1.0, 0.0,
    1.0, 1000000000000, /* To be replaced */
    0.0, 1.0
  };
  double** coords;
  double range[2];
  size_t nverts[] = { 4 };
  size_t ncomps = 1;
  struct mem_allocator allocator;
  struct polygon_context ctx;
  struct scpr_polygon* polygon;
  struct scpr_polygon* expected;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  int eq;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  args.allocator = &allocator;
  OK(scpr_device_create(&args, &dev));

  /* Set barely in-range value in coords2 */
  SCPR(device_get_range(dev, range));
  coords2[5] = coords2_reverse[5] = range[1];

  coords = (double**)MEM_CALLOC(&allocator, ncomps, sizeof(*coords));
  *coords = (double*)MEM_CALLOC(&allocator, nverts[0], 2*sizeof(**coords));
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));

  OK(scpr_polygon_create(dev, &polygon));
  OK(scpr_polygon_create(dev, &expected));

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.ncomps = ncomps;

  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  /* Offset 0 = unchanged */
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Offset 1 */
  memcpy(*coords, coords1, 2*nverts[0]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, 1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Offset -1: back to original polygon */
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Non representable offset: truncation will ensure stability */
  OK(scpr_offset_polygon(polygon, 0.123456789, SCPR_JOIN_MITER));
  OK(scpr_offset_polygon(expected, 0.123457, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Offset -5: empty polygon */
  ncomps = 0;
  ctx.ncomps = ncomps;
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -5, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Check coordinates barely in-range */
  memcpy(*coords, coords2, 2*nverts[0]*sizeof(**coords));

  ncomps = 1;
  ctx.ncomps = ncomps;

  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  /* Offset 0 = unchanged */
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Check non-effect of CW/CCW */
  memcpy(*coords, coords2_reverse, 2*nverts[0]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);
  CHK(check_stability(dev, polygon));

  /* Check out of range after offset being detected */
  BAD(scpr_offset_polygon(polygon, 1, SCPR_JOIN_MITER));

  /* Cleanup */
  OK(scpr_polygon_ref_put(polygon));
  OK(scpr_device_ref_put(dev));
  OK(scpr_polygon_ref_put(expected));

  MEM_RM(&allocator, *coords);
  MEM_RM(&allocator, coords);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
}

static void
test_double(void)
{
  const double coords0[] = {
    0.0, 0.0,
    0.0, 1.0,
    1.0, 1.0,
    1.0, 0.0
  };
  const double coords1[] = {
    10.0, 0.0,
    10.0, 1.0,
    11.0, 1.0,
    11.0, 0.0
  };
  const double coords2[] = {
    -1.0, -1.0,
    -1.0, 2.0,
    2.0, 2.0,
    2.0, -1.0
  };
  const double coords3[] = {
    9.0, -1.0,
    9.0, 2.0,
    12.0, 2.0,
    12.0, -1.0
  };
  const double coords4[] = {
    -4.5, -4.5,
    -4.5, 5.5,
    15.5, 5.5,
    15.5, -4.5
  };
  double** coords;
  size_t nverts[] = { 4, 4 };
  size_t ncomps = 2;
  struct mem_allocator allocator;
  struct polygon_context ctx;
  struct scpr_polygon* polygon;
  struct scpr_polygon* expected;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  int eq;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  args.allocator = &allocator;
  OK(scpr_device_create(&args, &dev));

  coords = (double**)MEM_CALLOC(&allocator, ncomps, sizeof(*coords));
  *coords = (double*)MEM_CALLOC(&allocator, nverts[0], 2*sizeof(**coords));
  *(coords+1) = (double*)MEM_CALLOC(&allocator, nverts[1], 2*sizeof(**coords));
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords1, 2*nverts[1]*sizeof(**coords));

  OK(scpr_polygon_create(dev, &polygon));
  OK(scpr_polygon_create(dev, &expected));

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.ncomps = ncomps;

  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  /* Offset 0 = unchanged */
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset 1 */
  memcpy(*coords, coords2, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords3, 2*nverts[1]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, 1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset -1: back to original polygon */
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords1, 2*nverts[1]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset 4.5: the 2 squares merge */
  ncomps = 1;
  ctx.ncomps = ncomps;
  memcpy(*coords, coords4, 2*nverts[0]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, 4.5, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset -5: empty polygon */
  ncomps = 0;
  ctx.ncomps = ncomps;
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -5, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  OK(scpr_polygon_ref_put(polygon));
  OK(scpr_device_ref_put(dev));
  OK(scpr_polygon_ref_put(expected));

  MEM_RM(&allocator, *coords);
  MEM_RM(&allocator, *(coords+1));
  MEM_RM(&allocator, coords);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
}

static void
test_internal(void)
{
  const double coords0[] = {
    -10, -10,
    -10, 10,
    10, 10,
    10, -10
  };
  const double coords1[] = {
    -5, -5,
    5, -5,
    5, 5,
    -5, 5
  };
  const double coords2[] = {
    -9, -9,
    -9, 9,
    9, 9,
    9, -9
  };
  const double coords3[] = {
    -6, -6,
    6, -6,
    6, 6,
    -6, 6
  };
  const double coords4[] = {
    -15, -15,
    -15, 15,
    15, 15,
    15, -15
  };
  double** coords;
  size_t nverts[] = { 4, 4 };
  size_t ncomps = 2;
  struct mem_allocator allocator;
  struct polygon_context ctx;
  struct scpr_polygon* polygon;
  struct scpr_polygon* expected;
  struct scpr_device_create_args args = SCPR_DEVICE_CREATE_ARGS_DEFAULT;
  struct scpr_device* dev;
  int eq;

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);
  args.allocator = &allocator;
  OK(scpr_device_create(&args, &dev));

  coords = (double**)MEM_CALLOC(&allocator, ncomps, sizeof(*coords));
  *coords = (double*)MEM_CALLOC(&allocator, nverts[0], 2*sizeof(**coords));
  *(coords+1) = (double*)MEM_CALLOC(&allocator, nverts[1], 2*sizeof(**coords));
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords1, 2*nverts[1]*sizeof(**coords));

  OK(scpr_polygon_create(dev, &polygon));
  OK(scpr_polygon_create(dev, &expected));

  ctx.coords = coords;
  ctx.nverts = nverts;
  ctx.ncomps = ncomps;

  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  /* Offset 0 = unchanged */
  OK(scpr_offset_polygon(polygon, 0, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset -1 */
  memcpy(*coords, coords3, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords2, 2*nverts[1]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset 1: back to original polygon */
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords1, 2*nverts[1]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, 1, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* Offset 5: internal path disappears */
  ncomps = 1;
  ctx.ncomps = ncomps;
  memcpy(*coords, coords4, 2*nverts[0]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, 5, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  /* From the original polygon, offset -2.5: empty polygon */
  ncomps = 2;
  ctx.ncomps = ncomps;
  memcpy(*coords, coords0, 2*nverts[0]*sizeof(**coords));
  memcpy(*(coords+1), coords1, 2*nverts[1]*sizeof(**coords));
  OK(scpr_polygon_setup_indexed_vertices(polygon, ncomps, pget_nverts, pget_pos, &ctx));

  ncomps = 0;
  ctx.ncomps = ncomps;
  OK(scpr_polygon_setup_indexed_vertices(expected, ncomps, pget_nverts, pget_pos, &ctx));

  OK(scpr_offset_polygon(polygon, -2.5, SCPR_JOIN_MITER));
  OK(scpr_polygon_eq(polygon, expected, &eq));
  CHK(eq);

  OK(scpr_polygon_ref_put(polygon));
  OK(scpr_polygon_ref_put(expected));
  OK(scpr_device_ref_put(dev));

  MEM_RM(&allocator, *coords);
  MEM_RM(&allocator, *(coords+1));
  MEM_RM(&allocator, coords);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
}

int
main(int argc, char** argv)
{
  (void)argc; (void)argv;
  test_single();
  test_double();
  test_internal();
  return 0;
}
