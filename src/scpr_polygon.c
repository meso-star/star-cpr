/* Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "scpr.h"
#include "scpr_c.h"

#include <polygon.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>
#include <rsys/float2.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static FINLINE Clipper2Lib::JoinType
scpr_join_type_to_clipper_join_type(const enum scpr_join_type t)
{
  Clipper2Lib::JoinType jtype;
  switch(t) {
    case SCPR_JOIN_SQUARE: jtype = Clipper2Lib::JoinType::Square; break;
    case SCPR_JOIN_ROUND: jtype = Clipper2Lib::JoinType::Round; break;
    case SCPR_JOIN_MITER: jtype = Clipper2Lib::JoinType::Miter; break;
    default: FATAL("Unreachable code\n"); break;
  }
  return jtype;
}

static void
polygon_release(ref_T* ref)
{
  struct scpr_polygon* polygon;
  struct mem_allocator* allocator;
  ASSERT(ref);
  polygon = CONTAINER_OF(ref, struct scpr_polygon, ref);
  allocator = polygon->device->allocator;
  SCPR(device_ref_put(polygon->device));
  /* Call destructor for paths */
  polygon->paths.Clipper2Lib::Paths64::~Paths64();
  MEM_RM(allocator, polygon);
}

static int
path_is_eq
  (const Clipper2Lib::Path64* p1,
   const Clipper2Lib::Path64* p2)
{
  size_t i, first_vtx, sz;
  int opposite_cw;
  ASSERT(p1 && p2);
  sz = p1->size();

  if(sz != p2->size()) {
    return 0;
  }
  FOR_EACH(opposite_cw, 0, 2) {
    FOR_EACH(first_vtx, 0, sz) {
      int eq = 1;
      FOR_EACH(i, 0, sz) {
       size_t n;
       if(opposite_cw) n = sz - 1 - (i + first_vtx) % sz;
       else n = (i + first_vtx) % sz;
        if((*p1)[i] != (*p2)[n]) {
          eq = 0;
          break; /* This opposite_cw/fstv failed: try next */
        }
      }
      /* Could prove p1 == p2 */
      if(eq) return 1;
    }
  }
  return 0;
}

static int
one_path_is_eq
  (const Clipper2Lib::Paths64* pp,
   const Clipper2Lib::Path64* p,
   char* matched)
{
  size_t i, sz;
  ASSERT(pp && p && matched);
  sz = pp->size();
  FOR_EACH(i, 0, sz) {
    if(matched[i]) continue;
    if(path_is_eq(&(*pp)[i], p)) {
     matched[i] = 1;
     return 1;
    }
  }
  return 0;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
scpr_polygon_create
  (struct scpr_device* dev,
   struct scpr_polygon** out_polygon)
{
  struct scpr_polygon* polygon = NULL;
  res_T res = RES_OK;

  if(!dev || !out_polygon) {
    res = RES_BAD_ARG;
    goto error;
  }

  polygon = (struct scpr_polygon*)
    MEM_CALLOC(dev->allocator, 1, sizeof(struct scpr_polygon));
  if(!polygon) {
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&polygon->ref);
  polygon->device = dev;
  SCPR(device_ref_get(dev));
  /* Allocate paths the C++ way (placement new) */
  new (&polygon->paths) Clipper2Lib::PathsD;
  polygon->lower[0] = polygon->lower[1] = INT64_MAX;
  polygon->upper[0] = polygon->upper[1] = INT64_MIN;

exit:
  if(out_polygon) *out_polygon = polygon;
  return res;

error:
  if(polygon) {
    SCPR(polygon_ref_put(polygon));
    polygon = NULL;
  }
  goto exit;
}

res_T
scpr_polygon_ref_get
  (struct scpr_polygon* polygon)
{
  if(!polygon) return RES_BAD_ARG;
  ref_get(&polygon->ref);
  return RES_OK;
}

res_T
scpr_polygon_ref_put
  (struct scpr_polygon* polygon)
{
  if(!polygon) return RES_BAD_ARG;
  ref_put(&polygon->ref, polygon_release);
  return RES_OK;
}

res_T
scpr_polygon_setup_indexed_vertices
  (struct scpr_polygon* polygon,
   const size_t ncomponents,
   void (*get_nverts)(const size_t icomponent, size_t *nverts, void* ctx),
   void (*get_position)
     (const size_t icomponent, const size_t ivert, double pos[2], void* ctx),
   void* data)
{
  size_t c;
  struct scpr_device* dev;
  Clipper2Lib::Paths64 paths;
  int changedc, changedv = 0;
  res_T res = RES_OK;

  if(!polygon || !get_nverts || !get_position || !data) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(ncomponents > UINT32_MAX) {
    logger_print(polygon->device->logger, LOG_ERROR,
        "Too many components.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  dev = polygon->device;
  TRY(paths.resize(ncomponents));

  FOR_EACH(c, 0, ncomponents) {
    size_t i, nverts;

    /* Get count for connex component c */
    get_nverts(c, &nverts, data);
    if(nverts > UINT32_MAX) {
      logger_print(polygon->device->logger, LOG_ERROR,
          "Too many vertices for component %zu.\n", c);
      res = RES_BAD_ARG;
      goto error;
    }
    TRY(paths[c].resize(nverts));

    /* Fetch polygon positions for connex component c */
    FOR_EACH(i, 0, nverts) {
      double tmp[2];
      int64_t tmp64[2];
      Clipper2Lib::Point64 pt;
      get_position(c, i, tmp, data);
      /* Check range and truncate precision to ensure further consistency */
      ERR(scpr_device_scale(dev, tmp, 2, tmp64));
      pt.x = tmp64[0];
      pt.y = tmp64[1];
      paths[c][i] = pt;
    }
  }

  /* Merge vertices, ... */
  TRY(polygon->paths = Clipper2Lib::SimplifyPaths(paths, dev->inv_scale));
  changedc = (ncomponents != polygon->paths.size());
  for(c = 0; !changedv && c < polygon->paths.size(); c++) {
    size_t nv;
    get_nverts(c, &nv, data);
    changedv |= (nv != polygon->paths[c].size());
  }
  if(changedc || changedv) {
    /* TODO: emit a warning log */
    logger_print(dev->logger, LOG_WARNING,
        "Polygon has been simplified. Original counts are no longer valid.\n");
  }

  /* Build bounding box (don't use ncomponents nor get_nverts just in case
   * counts are no longer valid!) */
  polygon->lower[0] = polygon->lower[1] = INT64_MAX;
  polygon->upper[0] = polygon->upper[1] = INT64_MIN;
  FOR_EACH(c, 0, polygon->paths.size()) {
    size_t i;
    FOR_EACH(i, 0, polygon->paths[c].size()) {
      int64_t tmp[2];
      int j;
      tmp[0] = polygon->paths[c][i].x;
      tmp[1] = polygon->paths[c][i].y;
      for(j = 0; j < 2; j++) {
        if(polygon->lower[j] > tmp[j]) polygon->lower[j] = tmp[j];
        if(polygon->upper[j] < tmp[j]) polygon->upper[j] = tmp[j];
      }
    }
  }

exit:
  return res;
error:
  if(polygon) {
    polygon->paths.clear();
    polygon->lower[0] = polygon->lower[1] = INT64_MAX;
    polygon->upper[0] = polygon->upper[1] = INT64_MIN;
  }
  goto exit;
}

res_T
scpr_polygon_in_bbox
  (struct scpr_polygon* polygon,
   const double lower[2],
   const double upper[2],
   int* inside)
{
  int i, in = 1;
  int64_t low[2], up[2];
  res_T res = RES_OK;

  if(!polygon || !lower || !upper || !inside) {
    res = RES_BAD_ARG;
    goto error;
  }

  SCPR(device_scale(polygon->device, lower, 2, low));
  SCPR(device_scale(polygon->device, upper, 2, up));
  for(i = 0; i < 2; i++) {
    if(polygon->lower[i] < low[i] || polygon->upper[i] > up[i]) {
      in = 0;
      break;
    }
  }

  *inside = in;
exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_create_copy
  (struct scpr_device* dev,
   const struct scpr_polygon* src_polygon,
   struct scpr_polygon** out_polygon)
{
  struct scpr_polygon* copy = NULL;
  int i;
  res_T res = RES_OK;

  if(!dev || !src_polygon || !out_polygon) {
    res = RES_BAD_ARG;
    goto error;
  }
  ERR(scpr_polygon_create(dev, &copy));

  copy->paths = src_polygon->paths;
  for(i = 0; i < 2; i++) {
    copy->lower[i] = src_polygon->lower[i];
    copy->upper[i] = src_polygon->upper[i];
  }

exit:
  if(out_polygon) *out_polygon = copy;
  return res;
error:
  if(copy) SCPR(polygon_ref_put(copy));
  copy = NULL;
  goto exit;
}

res_T
scpr_polygon_get_components_count
  (const struct scpr_polygon* polygon,
   size_t* ncomps)
{
  if(!polygon || !ncomps) {
    return RES_BAD_ARG;
  }
  *ncomps = polygon->paths.size();
  return RES_OK;
}

res_T
scpr_polygon_get_vertices_count
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   size_t* nverts)
{
  if(!polygon || !nverts || icomponent >= polygon->paths.size()) {
    return RES_BAD_ARG;
  }
  *nverts = polygon->paths[icomponent].size();
  return RES_OK;
}

res_T
scpr_polygon_get_position
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   const size_t ivert,
   double pos[2])
{
  size_t nverts;
  const size_t i = ivert;
  const Clipper2Lib::Point64* pt;
  int64_t pos64[2];
  if(!polygon || !pos || icomponent >= polygon->paths.size()) {
    return RES_BAD_ARG;
  }
  SCPR(polygon_get_vertices_count(polygon, icomponent, &nverts));
  if(ivert >= nverts) return RES_BAD_ARG;
  pt = &polygon->paths[icomponent][i];
  pos64[0] = pt->x;
  pos64[1] = pt->y;
  SCPR(device_unscale(polygon->device, pos64, 2, pos));
  return RES_OK;
}

res_T
scpr_offset_polygon
  (struct scpr_polygon* poly_desc,
   const double offset, /* Can be either positive or negative */
   const enum scpr_join_type join_type)
{
  size_t c;
  Clipper2Lib::Path64 tmp;
  struct scpr_device* dev;
  Clipper2Lib::JoinType cjt;
  Clipper2Lib::Paths64 polygon;
  int64_t offset64;
  res_T res = RES_OK;

  if(!poly_desc) {
    res = RES_BAD_ARG;
    goto error;
  }

  dev = poly_desc->device;

  /* Check offset polygon will be in-range */
  if(offset > 0) {
    int i;
    double range[2];
    SCPR(device_get_range(dev, range));
    for(i = 0; i < 2; i++) {
      if((double)poly_desc->lower[i] - offset < range[0]
          || (double)poly_desc->upper[i] + offset > range[1])
      {
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }

  /* Check join type */
  switch(join_type) {
    case SCPR_JOIN_SQUARE:
    case SCPR_JOIN_ROUND:
    case SCPR_JOIN_MITER:
      break;
    default:
      res = RES_BAD_ARG;
      goto error;
  }

  ERR(scpr_device_scale(dev, &offset, 1, &offset64));
  cjt = scpr_join_type_to_clipper_join_type(join_type);
  TRY(poly_desc->paths = Clipper2Lib::InflatePaths(poly_desc->paths,
        (double)offset64, cjt, Clipper2Lib::EndType::Polygon));

  /* Rebuild AABB */
  poly_desc->lower[0] = poly_desc->lower[1] = INT64_MAX;
  poly_desc->upper[0] = poly_desc->upper[1] = INT64_MIN;
  FOR_EACH(c, 0, poly_desc->paths.size()) {
    size_t i, nverts;
    nverts = poly_desc->paths[c].size();

    FOR_EACH(i, 0, nverts) {
      int64_t pos64[2];
      int j;
      pos64[0] = poly_desc->paths[c][i].x;
      pos64[1] = poly_desc->paths[c][i].y;
      for(j = 0; j < 2; j++) {
        if(poly_desc->lower[j] > pos64[j]) poly_desc->lower[j] = pos64[j];
        if(poly_desc->upper[j] < pos64[j]) poly_desc->upper[j] = pos64[j];
      }
    }
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_is_component_cw
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   int* cw)
{
  res_T res = RES_OK;

  if(!polygon || !cw || icomponent > polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  *cw = !Clipper2Lib::IsPositive(polygon->paths[icomponent]);

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_reverse_component
  (struct scpr_polygon* polygon,
   const size_t icomponent)
{
  res_T res = RES_OK;
  Clipper2Lib::Point64* data;
  size_t i, j;

  if(!polygon || icomponent > polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  i = 0;
  j = polygon->paths[icomponent].size();
  data = polygon->paths[icomponent].data();
  while(i != j && i != --j) {
    SWAP(Clipper2Lib::Point64, data[i], data[j]);
    i++; /* Not in SWAP macro! */
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_eq
  (const struct scpr_polygon* polygon1,
   const struct scpr_polygon* polygon2,
   int* is_eq)
{
  size_t i, sz;
  char* matched = NULL;
  res_T res = RES_OK;

  if(!polygon1 || !polygon2 || !is_eq) {
    res = RES_BAD_ARG;
    goto error;
  }

  sz = polygon1->paths.size();
  if(sz != polygon2->paths.size()) {
    *is_eq = 0;
    goto exit;
  }
  if(polygon1->lower[0] != polygon2->lower[0]
      || polygon1->lower[1] != polygon2->lower[1]
      || polygon1->upper[0] != polygon2->upper[0]
      || polygon1->upper[1] != polygon2->upper[1])
  {
    *is_eq = 0;
    goto exit;
  }

  /* Check actual coordinates */
  matched = (char*)calloc(sz, sizeof(*matched));
  FOR_EACH(i, 0, sz) {
    if(!one_path_is_eq(&polygon1->paths, &polygon2->paths[i], matched)) {
      *is_eq = 0;
      goto exit;
    }
  }
  *is_eq = 1;

exit:
  free(matched);
  return res;
error:
  goto exit;
}

res_T
scpr_get_vertex_in_component
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   double vertex[2])
{
  size_t i, p1sz;
  Clipper2Lib::Point64 p0, c;
  Clipper2Lib::PointInPolygonResult in;
  res_T res = RES_OK;

  if(!polygon || !vertex || icomponent >= polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Find the center of the segment between vertex #0 and vertex #i
   * If it is inside the polygon return it */
  p0 = polygon->paths[icomponent][0];
  p1sz = polygon->paths[icomponent].size();
  for(i = 2; i < polygon->paths[0].size(); i++) {
    Clipper2Lib::Point64 pi = polygon->paths[icomponent][i];
    if(p1sz == 3) {
      /* Special case of a triangle: get the barycenter */
      Clipper2Lib::Point64 p1 = polygon->paths[icomponent][1];
      c = (p0 + p1 + pi) * 0.3333333;
    } else {
      c = (p0 + pi) * 0.5;
    }
    TRY(in = Clipper2Lib::PointInPolygon(c, polygon->paths[icomponent]));
    if(in == Clipper2Lib::PointInPolygonResult::IsOn) {
      int64_t tmp64[2];
      tmp64[0] = c.x; tmp64[1] = c.y;
      ERR(scpr_device_unscale(polygon->device, tmp64, 2, vertex));
      break; /* Found! */
    }
  }
  /* Should not be there: the component is either ill-formed or too thin to host
   * a vertex in its inside! */
  res = RES_BAD_ARG;

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_is_vertex_in_component
  (const struct scpr_polygon* polygon,
   const size_t icomponent,
   const double vertex[2],
   int* situation)
{
  res_T res = RES_OK;
  int64_t tmp64[2];
  Clipper2Lib::Point64 pt;
  Clipper2Lib::PointInPolygonResult in;

  if(!polygon || !vertex || !situation || icomponent >= polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  ERR(scpr_device_scale(polygon->device, vertex, 2, tmp64));
  TRY(pt.Init(SPLIT2(tmp64)));
  TRY(in = Clipper2Lib::PointInPolygon(pt, polygon->paths[icomponent]));
  switch(in) {
    case Clipper2Lib::PointInPolygonResult::IsOn:
      *situation = 0;
      break;
    case Clipper2Lib::PointInPolygonResult::IsOutside:
      *situation = -1;
      break;
    case Clipper2Lib::PointInPolygonResult::IsInside:
      *situation = +1;
      break;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_is_component_in_component
  (const struct scpr_polygon* polygon1,
   const size_t icomponent1,
   const struct scpr_polygon* polygon2,
   const size_t icomponent2,
   int* c1_is_in_c2)
{
  size_t i, p1sz;
  int is_in = -1;
  Clipper2Lib::PointInPolygonResult in;
  const Clipper2Lib::Path64* comp1;
  const Clipper2Lib::Path64* comp2;
  res_T res = RES_OK;

  if(!polygon1 || icomponent1 >= polygon1->paths.size()
     || !polygon2 || icomponent2 >= polygon2->paths.size()
     || ! c1_is_in_c2)
  {
    res = RES_BAD_ARG;
    goto error;
  }

  /* comp1 == comp2 is reported as bad arg.
   * This API requires comp1 and comp2 to not overlap (they can be adjoining),
   * this allows to exit early as soon as 1 vertex is either inside or outside */
  comp1 = &polygon1->paths[icomponent1];
  comp2 = &polygon2->paths[icomponent2];
  p1sz = comp1->size();
  for(i = 0; i < p1sz; i++) {
    Clipper2Lib::Point64 p = (*comp1)[i];
    TRY(in = Clipper2Lib::PointInPolygon(p, (*comp2)));
    switch(in) {
      case Clipper2Lib::PointInPolygonResult::IsOutside:
        is_in = 0;
        break;
      case Clipper2Lib::PointInPolygonResult::IsOn:
        /* Cannot decide based on this */
        break;
      case Clipper2Lib::PointInPolygonResult::IsInside:
        is_in = 1;
        break;
    }
    if(is_in >= 0) break; /* Early exit */
  }
  if(is_in == -1) {
    /* Every vertex of comp1 is on comp2: comp1 is either equal to comp2, or it
     * is inside */
    int is_eq = path_is_eq(comp1, comp2);
    if(!is_eq) {
      is_in = 1;
    } else {
      res = RES_BAD_ARG;
      goto error;
    }
  }

  * c1_is_in_c2 = is_in;

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_dump_to_obj
  (struct scpr_polygon* polygon,
   FILE* stream)
{
  res_T res = RES_OK;
  size_t i, j, n = 1;

  if(!polygon || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Vertice */
  for(i = 0; i < polygon->paths.size(); i++) {
    for(j = 0; j < polygon->paths[i].size(); j++) {
      double tmpd[2];
      float tmpf[2];
      int64_t tmp64[2];
      Clipper2Lib::Point64& pt = polygon->paths[i][j];
      tmp64[0] = pt.x;
      tmp64[1] = pt.y;
      ERR(scpr_device_unscale(polygon->device, tmp64, 2, tmpd));
      f2_set_d2(tmpf, tmpd);
      fprintf(stream, "v %.16g %.16g 0\n", tmpf[0], tmpf[1]);
    }
  }

  /* Lines */
  for(i = 0; i < polygon->paths.size(); i++) {
    size_t fst = n;
    fprintf(stream, "l ");
    for(j = 0; j < polygon->paths[i].size(); j++) {
      fprintf(stream, " %zu", n++);
    }
    fprintf(stream, " %zu\n", fst);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
scpr_polygon_dump_component_to_obj
  (struct scpr_polygon* polygon,
   const size_t icomponent,
   FILE* stream)
{
  res_T res = RES_OK;
  size_t i;

  if(!polygon || !stream || icomponent >= polygon->paths.size()) {
    res = RES_BAD_ARG;
    goto error;
  }

  /* Vertice */
  for(i = 0; i < polygon->paths[icomponent].size(); i++) {
    double tmpd[2];
    float tmpf[2];
    int64_t tmp64[2];
    Clipper2Lib::Point64& pt = polygon->paths[icomponent][i];
    tmp64[0] = pt.x;
    tmp64[1] = pt.y;
    ERR(scpr_device_unscale(polygon->device, tmp64, 2, tmpd));
    f2_set_d2(tmpf, tmpd);
    fprintf(stream, "v %.16g %.16g 0\n", tmpf[0], tmpf[1]);
  }

  /* Line */
  fprintf(stream, "l ");
  for(i = 0; i < polygon->paths[icomponent].size(); i++) {
    fprintf(stream, " %zu", ++i);
  }
  fprintf(stream, " 1\n");

exit:
  return res;
error:
  goto exit;
}
