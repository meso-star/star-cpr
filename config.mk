VERSION = 0.5.1
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
CXX = c++
LD = ld
OBJCOPY = objcopy
PKG_CONFIG = pkg-config
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

CLIPPER2_VERSION = 1.4
CLIPPER2_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags Clipper2)
CLIPPER2_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs Clipper2)

POLYGON_VERSION = 0.2
POLYGON_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags polygon)
POLYGON_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs polygon)

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

DPDC_CFLAGS = $(CLIPPER2_CFLAGS) $(POLYGON_CFLAGS) $(RSYS_CFLAGS)
DPDC_LIBS = $(CLIPPER2_LIBS) $(POLYGON_LIBS) $(RSYS_LIBS)

################################################################################
# Compilation options
################################################################################
FLAGS =\
 -fvisibility=hidden\
 -fstrict-aliasing\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_COMMON =\
 -std=c89\
 -Wmissing-prototypes\
 $(CFLAGS_HARDENED)\
 $(FLAGS)

CXXFLAGS_COMMON =\
 -std=c++17\
 $(CFLAGS_HARDENED)\
 $(FLAGS)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE)) -fPIE # C is only used for the tests

CXXFLAGS_DEBUG = -g $(CXXFLAGS_COMMON)
CXXFLAGS_RELEASE = -O2 -DNDEBUG $(CXXFLAGS_COMMON)
CXXFLAGS = $(CXXFLAGS_$(BUILD_TYPE)) -fPIC # C++ is only used for the library

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
