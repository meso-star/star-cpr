# Star-CliPpeR

This library clips triangulated 2D meshes against 2D polygons. Two
clipping operations between the mesh and the clipping region are
provided: the subtraction and the intersection. The former removes the
clipping region from the original mesh while the latter keeps only the
mesh part that intersects the clipping polygon.

## Requirements

- C++ compiler (C++17)
- POSIX make
- pkg-config
- [Clipper2](https://github.com/AngusJohnson/Clipper2),
- [Polygon](https://gitlab.com/vaplv/polygon)
- [RSys](https://gitlab.com/vaplv/rsys)
- C compiler (optional for tests)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.5.1

  - Upgrade Clipper2 dependency to 1.4.0
    Move to the make build system reverted Clipper2 to obsolete 1.1.1

### Version 0.5

- Replace CMake with POSIX make as build system.
- Provide a pkg-config file to link the library as an external
  dependency.
- Add compiler and linker flags to increase the security and robustness
  of the generated library.

### Version 0.4.1

- Upgrade Clipper2 dependency to 1.3.0.

### Version 0.4

- Add `scpr_is_vertex_in_component`, `scpr_is_component_in_component`
  and `scpr_get_vertex_in_component` to check for polygons inclusion.
- Add `scpr_polygon_is_component_cw` and
  `scpr_polygon_reverse_component` API calls to manage polygon
  orientation.
- Fix OBJ output.

### Version 0.3

- Add functions to detect polygons intersections. At this stage, only
  intersections of segments ares detected (neither "vertex on a
  segment", nor "polygons sharing a vertex" are detected).
- Add a device to hold library settings; this breaks the whole API.
- Change internals to use integer representation for polygons.
- Improve out-of-range detection.
- Upgrade Clipper2 to 1.2.0

### Version 0.2

- Switch Clipper library to Clipper2. This increases requirements on
  compiler to C++17.
- Allow complex polygons (polygons with multiple components, defining
  holes).
- Add a function to offset polygons.
- Make constraints on coordinates explicit: limited range and limited
  precision.

### Version 0.1.3

- Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8
  has become obsolete.
- Fix compilation warnings detected by gcc 11.

### Version 0.1.2

Update CMake module of the Clipper library: on GNU/Linux, make optional
the debug version of the library.

### Version 0.1.1

Update the version of the RSys dependency to 0.6: replace the deprecated
`[N]CHECK` macros by the new macro `CHK`.

## License

Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)

Star-CliPpeR is free software released under the GPL v3+ license: GNU
GPL version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
