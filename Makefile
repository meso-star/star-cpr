# Copyright (C) 2016-2018, 2021-2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = libscpr.a
LIBNAME_SHARED = libscpr.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC =\
 src/scpr_device.c\
 src/scpr_intersector.c\
 src/scpr_mesh.c\
 src/scpr_polygon.c
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library: .config $(DEP)
	@$(MAKE) -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CXX) $(CXXFLAGS) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): libscpr.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

libscpr.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(CLIPPER2_VERSION) Clipper2; then \
	  echo "Clipper2 $(CLIPPER2_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(POLYGON_VERSION) polygon; then \
	  echo "polygon $(POLYGON_VERSION) not found" >&2; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found" >&2; exit 1; fi
	@echo "config done" > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CXX) $(CXXFLAGS) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CXX) $(CXXFLAGS) $(DPDC_CFLAGS) -DSCPR_SHARED_BUILD -c $< -o $@

################################################################################
# Installation
################################################################################
pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@CLIPPER2_VERSION@#$(CLIPPER2_VERSION)#g'\
	    -e 's#@POLYGON_VERSION@#$(POLYGON_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    scpr.pc.in > scpr.pc

scpr-local.pc: scpr.pc.in
	sed -e '1d'\
	    -e 's#^includedir=.*#includedir=./src/#'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g'\
	    -e 's#@CLIPPER2_VERSION@#$(CLIPPER2_VERSION)#g'\
	    -e 's#@POLYGON_VERSION@#$(POLYGON_VERSION)#g'\
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g'\
	    scpr.pc.in > $@

install: build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" scpr.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/star" src/scpr.h
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/star-cpr" COPYING README.md

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/lib/$(LIBNAME)"
	rm -f "$(DESTDIR)$(PREFIX)/lib/pkgconfig/scpr.pc"
	rm -f "$(DESTDIR)$(PREFIX)/include/star/scpr.h"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-cpr/COPYING"
	rm -f "$(DESTDIR)$(PREFIX)/share/doc/star-cpr/README.md"

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .test libscpr.o scpr.pc scpr-local.pc

distclean: clean
	rm -f $(DEP) $(TEST_DEP)

lint:
	shellcheck -o all make.sh

################################################################################
# Tests
################################################################################
TEST_SRC =\
 src/test_scpr_clip.c\
 src/test_scpr_device.c\
 src/test_scpr_intersector.c\
 src/test_scpr_is_in.c\
 src/test_scpr_mesh.c\
 src/test_scpr_offset.c\
 src/test_scpr_polygon.c
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
SCPR_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags scpr-local.pc)
SCPR_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs scpr-local.pc)

build_tests: build_library $(TEST_DEP) .test
	@$(MAKE) -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) \
	test_bin

test: build_tests
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test: Makefile
	@$(SHELL) make.sh config_test $(TEST_SRC) > $@

clean_test:
	$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk scpr-local.pc
	@$(CC) $(CFLAGS) $(RSYS_CFLAGS) $(SCPR_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk scpr-local.pc
	$(CC) $(CFLAGS) $(RSYS_CFLAGS) $(SCPR_CFLAGS) -c $(@:.o=.c) -o $@

test_scpr_clip\
test_scpr_device\
test_scpr_intersector\
test_scpr_is_in\
test_scpr_mesh\
test_scpr_offset\
test_scpr_polygon \
: config.mk scpr-local.pc $(LIBNAME)
	$(CC) $(CFLAGS) -o $@ src/$@.o $(LDFLAGS_EXE) $(SCPR_LIBS) $(RSYS_LIBS) -lm
